package com.music.dianyuetai.db;

import android.util.Log;

import com.music.dianyuetai.App;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.bean.local.LocalMusic;

import java.util.List;

import static com.music.dianyuetai.download.DownloadManager.COMPLETED;
import static com.music.dianyuetai.download.DownloadManager.FAILURE;
import static com.music.dianyuetai.download.DownloadManager.LOADING;
import static com.music.dianyuetai.download.DownloadManager.PAUSE;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月02日-10时19分
 */

public class DaoManager {

    private static volatile DaoManager instance = null;

    private DaoSession daoSession;

    private LocalMusicDao musicDao;
    private LocalHistoryDao historyDao;
    private DownloadDao downloadDao;


    private DaoManager() {
        daoSession = App.getAppContext().getDaoSession();
        musicDao = daoSession.getLocalMusicDao();
        historyDao = daoSession.getLocalHistoryDao();
        downloadDao = daoSession.getDownloadDao();
    }

    public static DaoManager getInstance() {
        if (instance == null) {
            synchronized (DaoManager.class) {
                if (instance == null) {
                    instance = new DaoManager();
                }
            }
        }

        return instance;
    }

    /*#####################################################################################################################*/
    /*###############################################       歌曲本地记录     #################################################*/
    /*#####################################################################################################################*/

    /**
     * 新增一条歌曲记录
     *
     * @param music
     * @return
     */
    public long insertMusic(LocalMusic music) {
        return musicDao.insert(music);
    }

    /**
     * 新增或更新一条歌曲记录
     *
     * @param music
     * @return
     */
    public long updateOrInsertMusic(LocalMusic music) {
        LocalMusic unique = getMusic(music.getMusicId());
        if (unique != null) {
            music.setId(unique.getId());
            updateMusic(music);
            return unique.getId();
        } else {
            return insertMusic(music);
        }
    }

    /**
     * 更新一条歌曲记录
     *
     * @param music
     */
    public void updateMusic(LocalMusic music) {
        musicDao.update(music);
    }

    /**
     * 获取一条歌曲记录
     *
     * @param musicId 歌曲ObjectId
     * @return
     */
    public LocalMusic getMusic(String musicId) {
        LocalMusic music = null;
        try {
            music = musicDao.queryBuilder()
                    .where(LocalMusicDao.Properties.MusicId.eq(musicId))
                    .unique();
        } catch (Exception e) {

        }
        return music;
    }

    /**
     * 获取歌曲播放列表
     *
     * @return
     */
    public List<LocalMusic> getPlayList() {
        return musicDao.queryBuilder()
                .where(LocalMusicDao.Properties.IsInList.eq(true))
                .build()
                .list();
    }

    /**
     * 添加歌曲到播放列表
     *
     * @param music
     */
    public void insertPlay(LocalMusic music) {
        updateOrInsertMusic(music);
    }

    /**
     * 将一首歌从播放列表中移除
     *
     * @param musicId
     */
    public void deletePlay(String musicId) {
        LocalMusic music = getMusic(musicId);
        if (music != null) {
            music.setIsInList(false);
            updateMusic(music);
        }
    }

    /**
     * 移除播放列表的所有歌曲
     */
    public void deleteAllPlay() {
        List<LocalMusic> playMusicList = musicDao.queryBuilder()
                .where(LocalMusicDao.Properties.IsInList.eq(true))
                .build()
                .list();

        if (playMusicList != null && playMusicList.size() > 0) {
            int size = playMusicList.size();
            for (int i = 0; i < size; i++) {
                LocalMusic music = playMusicList.get(i);
                music.setIsInList(false);
                updateMusic(music);
            }
        }
    }

    /*#####################################################################################################################*/
    /*###############################################       下载记录     #################################################*/
    /*#####################################################################################################################*/

    /**
     * 新增一个下载任务
     *
     * @param download
     */
    public void insertDownload(Download download) {
        downloadDao.insert(download);
    }

    /**
     * 新增或更新一个下载任务
     *
     * @param download
     */
    public void updateOrInsertDownload(Download download) {
        Download unique = getDownload(download.getMusicObjectId());
        if (unique != null) {
            download.setId(unique.getId());
            download.setMusicId(unique.getMusicId());
            updateDownload(download);
        } else {
            insertDownload(download);
        }
    }

    /**
     * 更新下载任务
     *
     * @param download
     */
    public void updateDownload(Download download) {
        downloadDao.update(download);
    }

    /**
     * 获取一个下载任务
     *
     * @param musicObjectId 歌曲ObjectId
     * @return
     */
    public Download getDownload(String musicObjectId) {
        Download download = null;

        try {
            download = downloadDao.queryBuilder()
                    .where(DownloadDao.Properties.MusicObjectId.eq(musicObjectId))
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return download;
    }

    /**
     * 获取下载完成的任务列表
     *
     * @return
     */
    public List<Download> getDownloadedList() {
   /*     QueryBuilder<Download> queryBuilder = downloadDao.queryBuilder();
        queryBuilder
                .where(DownloadDao.Properties.DownloadState.eq(DownloadState.COMPLETED))
                .join(LocalMusic.class, LocalMusicDao.Properties.Id);
        return queryBuilder.list();*/

        List<Download> downloadList = downloadDao
                .queryBuilder()
                .where(DownloadDao.Properties.DownloadState.eq(COMPLETED))
                .list();

        Log.e("sdfsdfdsf", "下载完成列表大小：：：" + downloadList.size());
        return downloadList;
    }

    /**
     * 获取正在下载的任务列表
     *
     * @return
     */
    public List<Download> getDownloadingList() {
      /*  QueryBuilder<Download> queryBuilder = downloadDao.queryBuilder();
        queryBuilder
                .whereOr(DownloadDao.Properties.DownloadState.eq(DownloadState.DOWNLOADING), DownloadDao.Properties.DownloadState.eq(DownloadState.PAUSE))
                .join(LocalMusic.class, LocalMusicDao.Properties.Id);
        return queryBuilder.list();
*/
        List<Download> downloadingList = downloadDao
                .queryBuilder()
                .whereOr(DownloadDao.Properties.DownloadState.eq(LOADING),
                        DownloadDao.Properties.DownloadState.eq(PAUSE),
                        DownloadDao.Properties.DownloadState.eq(FAILURE))
                .list();

        Log.e("sdfsdfdsf", "下载中列表大小：：：" + downloadingList.size());
        return downloadingList;
    }

    public void deleteDownload(String musicId) {
        Download download = getDownload(musicId);
        if (download != null) {
            downloadDao.delete(download);
        }
    }

    /**
     * 删除一条下载任务
     *
     * @param download
     */
    public void deleteDownload(Download download) {
        if (download != null) {
            downloadDao.delete(download);
        }
    }

    public void deleteDownload(List<Download> downloadList) {
        if (downloadList != null) {
            downloadDao.deleteInTx(downloadList);
        }
    }

    /**
     * 删除所有下载任务
     */
    public void deleteAllDownload() {
        downloadDao.deleteAll();
    }

    /*#####################################################################################################################*/
    /*###############################################       历史记录     #################################################*/
    /*#####################################################################################################################*/

    /**
     * 插入一条历史记录
     *
     * @param history 历史记录实体
     */
    public void insertHistory(LocalHistory history) {
        historyDao.insert(history);
    }

    /**
     * 更新一条记录
     *
     * @param history 历史记录实体
     */
    public void updateHistory(LocalHistory history) {
        historyDao.update(history);
    }

    /**
     * 插入或更新一条记录
     *
     * @param history 历史记录实体
     */
    public void updateOrInsertHistory(LocalHistory history) {
        LocalHistory unique = getHistory(history.getMusicObjectId());
        if (unique != null) {
            history.setId(unique.getId());
            updateHistory(history);
            Log.e("更新记录", "更新记录");
        } else {
            insertHistory(history);
            Log.e("添加纪录", "添加纪录");
        }
    }

    /**
     * 获取单条记录
     *
     * @param musicObectId 歌曲ObectId
     * @return 记录实体对象
     */
    public LocalHistory getHistory(String musicObectId) {
        return historyDao.queryBuilder()
                .where(LocalHistoryDao.Properties.MusicObjectId.eq(musicObectId))
                .unique();
    }

    /**
     * 获取历史记录列表
     *
     * @return 历史记录集合
     */
    public List<LocalHistory> getHistoryList() {
       /* QueryBuilder<LocalHistory> query = historyDao.queryBuilder();*/
      /*  query.orderDesc(LocalHistoryDao.Properties.UpdateAt)
                .join(LocalMusic.class, LocalMusicDao.Properties.Id);*/
        return historyDao
                .queryBuilder()
                .orderDesc(LocalHistoryDao.Properties.UpdateAt)
                .list();
    }

    /**
     * 删除单条记录
     *
     * @param musicId 歌曲Id
     */
    public void deleteHistory(String musicId) {
        historyDao.queryBuilder()
                .where(LocalHistoryDao.Properties.MusicId.eq(musicId))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }

    /**
     * 删除所有记录
     */
    public void deleteAllHistory() {
        historyDao.deleteAll();
    }
}
