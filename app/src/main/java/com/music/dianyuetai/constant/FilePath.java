package com.music.dianyuetai.constant;

import android.os.Environment;

/**
 * Created by Tang on 2016/4/26 0026.
 */
public class FilePath {
    public static final String BASE_DIRECTORY = "/dianyuetai";

    //电悦台文件根目录
    public static String getBasePath() {
        return Environment.getExternalStorageDirectory() + BASE_DIRECTORY;
    }
}
