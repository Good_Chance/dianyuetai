package com.music.dianyuetai.constant;

/**
 * Created by Tang on 2016/5/24 0024.
 */
public class Rank {
    public static final String RECENTLY = "-created";
    public static final String MOST_PLAY = "-play";
    public static final String MOST_COMMENT = "-comments";
    public static final String MOST_FAVORITE = "-favorites";
}
