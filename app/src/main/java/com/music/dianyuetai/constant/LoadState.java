package com.music.dianyuetai.constant;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月25日-16时06分
 */

public class LoadState {
    public static final int FAILURE = -1;
    public static final int COMPLETE = 0;
    public static final int NOMORE = 1;
}
