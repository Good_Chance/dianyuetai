package com.music.dianyuetai.api;

import com.music.dianyuetai.bean.Comment;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import rx.Observable;
import rx.functions.Func1;


/**
 * Created by Tang on 2016/8/5 0005.
 */
public class CommentApi {
    //评论数据每页大小
    private static final int PAGE_SIZE = 10;

    private static final String FIELD_MUSIC_ID = "musicId";

    private static final String FIELD_USER = "user";

    /**
     * 获取歌曲的评论列表
     *
     * @param page          评论页码
     * @param musicObjectId 歌曲ObjId
     * @return
     */
    public static Observable<List<Comment>> getCommentList(int page, String musicObjectId) {
        if (page <= 0) {
            page = 1;
        }

        int skip = (page - 1) * PAGE_SIZE;
        BmobQuery<Comment> query = new BmobQuery<>();
        query.setSkip(skip);
        query.setCachePolicy(BmobQuery.CachePolicy.NETWORK_ONLY);
        query.setLimit(PAGE_SIZE);
        query.addWhereEqualTo(FIELD_MUSIC_ID, musicObjectId);
        query.include(FIELD_USER);
        return query.findObjectsObservable(Comment.class);
    }

    /**
     * 添加一条评论
     *
     * @param comment 评论实体
     * @return
     */
    public static Observable<String> addComment(Comment comment) {
        return comment.saveObservable();
    }

    /**
     * 赞一条评论
     *
     * @param commentObjectId 评论Objectid
     * @return
     */
    public static Observable<Void> increaseLike(String commentObjectId) {
        return updateCount(commentObjectId, 1);
    }

    /**
     * 取消赞一条评论
     *
     * @param commentObjectId 评论ObjectId
     * @return
     */
    public static Observable<Void> decreaseLike(String commentObjectId) {
        return updateCount(commentObjectId, -1);
    }

    /**
     * 更新评论支持数
     *
     * @param commentObjectId 评论ObjectId
     * @param count
     * @return
     */
    private static Observable<Void> updateCount(final String commentObjectId, final int count) {
        BmobQuery<Comment> query = new BmobQuery<>();
        return query.getObjectObservable(Comment.class, commentObjectId)
                .flatMap(new Func1<Comment, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(Comment comment) {
                        comment.setSupport(comment.getSupport() + count);
                        return comment.updateObservable();
                    }
                });
    }
}
