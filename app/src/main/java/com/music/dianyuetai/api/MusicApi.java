package com.music.dianyuetai.api;

import android.text.TextUtils;

import com.music.dianyuetai.bean.Music;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import cn.bmob.v3.BmobQuery;
import rx.Observable;
import rx.functions.Func1;


/**
 * 描述：歌曲列表相关Api
 * 作者：唐志远
 * 日期：2016年08月02日-15时28分
 */
public class MusicApi {
    //每页数据大小
    private static final int PAGE_SIZE = 10;

    //字段
    private static final String FIELD_PLAY = "playNum";
    private static final String FIELD_FAVORITE = "favoriteNum";
    private static final String FIELD_COMMENT = "commentNum";

    private static final String FIELD_RESOURCE = "resource";


    /**
     * 根据排序条件查询歌曲列表
     *
     * @param page  页码
     * @param order 排序条件
     * @return
     */
    public static Observable<List<Music>> getListByOrder(int page, String order) {
        return getMusicList(page, PAGE_SIZE, null, null, order, "");
    }

    /**
     * 根据歌曲风格查询歌曲列表
     *
     * @param page  页码
     * @param style 歌曲风格
     * @return
     */
    public static Observable<List<Music>> getListByCategory(int page, String style) {
        Map<String, Object> where = new Hashtable<>();
        where.put("style", style);

        return getMusicList(page, PAGE_SIZE, where, null, "", "");
    }

    /**
     * 搜索歌曲
     *
     * @return
     */
    public static Observable<List<Music>> getListBySelection(int page, String musicName) {
        return getMusicList(page, PAGE_SIZE, null, musicName, "", "");
    }

    /**
     * 获取歌曲列表
     *
     * @param page     页码
     * @param pageSize 每页大小
     * @param order    排序条件
     * @param keys     查询字段
     * @return
     */
    public static Observable<List<Music>> getMusicList(int page, int pageSize, Map<String, Object> where, String selection, String order, String keys) {
        if (page <= 0) {
            page = 1;
        }

        int skip = (page - 1) * pageSize;

        BmobQuery<Music> query = new BmobQuery<>();
        query.setSkip(skip);
        query.setLimit(pageSize);
        query.order(order);
        query.addQueryKeys(keys);
        query.include(FIELD_RESOURCE);

        if (where != null && where.size() > 0) {
            for (Map.Entry<String, Object> entrySet : where.entrySet()) {
                query.addWhereEqualTo(entrySet.getKey(), entrySet.getValue());
            }
        }
        if (!TextUtils.isEmpty(selection)) {
            query.addWhereContains("name", selection);
        }

        return query.findObjectsObservable(Music.class);
    }

    /**
     * 根据ObjectId查询歌曲
     *
     * @param objectid
     * @return
     */
    public static Observable<Music> getMusic(String objectid) {
        return getMusic(objectid, "");
    }

    /**
     * 根据ObjectId，指定字段，查询歌曲，
     *
     * @param objectId 歌曲ObjectId
     * @param keys     查询字段
     * @return
     */
    public static Observable<Music> getMusic(String objectId, String keys) {
        BmobQuery<Music> query = new BmobQuery<>();
        query.addQueryKeys(keys);
        query.include(FIELD_RESOURCE);
        return query.getObjectObservable(Music.class, objectId);
    }

    /**
     * 播放数加一
     *
     * @param objectId 歌曲ObjectId
     * @return
     */
    public static Observable<Void> increasePlay(String objectId) {
        return updateCount(objectId, FIELD_PLAY, 1);
    }

    /**
     * 收藏数加一
     *
     * @param objectId 歌曲ObjectId
     * @return
     */
    public static Observable<Void> increaseFavorite(String objectId) {
        return updateCount(objectId, FIELD_FAVORITE, 1);
    }

    /**
     * 收藏数减一
     *
     * @param objectId 歌曲ObjectId
     * @return
     */
    public static Observable<Void> decreaseFavorite(String objectId) {
        return updateCount(objectId, FIELD_FAVORITE, -1);
    }

    /**
     * 评论数加一
     *
     * @param objectId 歌曲ObjectId
     * @return
     */
    public static Observable<Void> increaseComment(String objectId) {
        return updateCount(objectId, FIELD_COMMENT, 1);
    }

    private static Observable<Void> updateCount(String objectId, final String keys, final int count) {
        return getMusic(objectId, keys)
                .flatMap(new Func1<Music, Observable<Void>>() {
                    @Override
                    public Observable<Void> call(Music music) {
                        switch (keys) {
                            case FIELD_PLAY:
                                music.setPlayNum(music.getPlayNum() + count);
                                break;
                            case FIELD_FAVORITE:
                                music.setFavoriteNum(music.getFavoriteNum() + count);
                                break;
                            case FIELD_COMMENT:
                                music.setFavoriteNum(music.getCommentNum() + count);
                                break;
                            default:
                                break;
                        }
                        return music.updateObservable();
                    }
                });
    }
}
