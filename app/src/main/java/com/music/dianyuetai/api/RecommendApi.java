package com.music.dianyuetai.api;

import com.music.dianyuetai.bean.Recommend;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import rx.Observable;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-15时09分
 */

public class RecommendApi {
    private static final int PAGE_SIZE = 10;

    public static Observable<List<Recommend>> getRecommendList() {
        return getRecommendList(1, 100);
    }

    public static Observable<List<Recommend>> getRecommendList(int page) {
        return getRecommendList(page, PAGE_SIZE);
    }

    public static Observable<List<Recommend>> getRecommendList(int page, int pageSize) {
        if (page <= 0) {
            page = 1;
        }
        int skip = (page - 1) * pageSize;

        BmobQuery<Recommend> query = new BmobQuery<>();
        query.setSkip(skip);
        query.setLimit(pageSize);
        query.include("music.resource");

        return query.findObjectsObservable(Recommend.class);
    }
}
