package com.music.dianyuetai.api;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.music.dianyuetai.bean.User;


import cn.bmob.v3.BmobUser;
import cn.bmob.v3.listener.UpdateListener;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-15时05分
 */

public class UserApi {

    /**
     * 登录
     *
     * @param account  用户名/手机号
     * @param password 密码
     * @return 用户实体
     */
    public static Observable<User> login(String account, String password) {
        return BmobUser.loginByAccountObservable(User.class, account, password);
    }

    /**
     * 登录，不是首次登陆
     *
     * @param user
     * @return
     */
    public static Observable<User> login(User user) {
        return user.loginObservable(User.class);
    }

    public static Observable<User> register(String account, String nickName, String password) {
        User user = new User();
        user.setUsername(account);
        user.setPassword(password);
        user.setNickName(nickName);
        return user.signUpObservable(User.class);
    }

    public static Subscription updatePassword(String oldPassword, String newPassword, UpdateListener listener) {
        return BmobUser.updateCurrentUserPassword(oldPassword, newPassword, listener);
    }

}
