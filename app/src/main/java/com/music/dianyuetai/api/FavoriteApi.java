package com.music.dianyuetai.api;

import com.music.dianyuetai.bean.Favorite;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Tang on 2016/8/5 0005.
 */
public class FavoriteApi {
    private static final String FIELD_USER_ID = "userObjId";
    private static final String FIELD_MUSIC_ID = "musicId";

    private static final String FIELD_MUSIC = "music";

    /**
     * 查询用户的收藏列表
     *
     * @param userObjectId 用户ObjectId
     * @return
     */
    public static Observable<List<Favorite>> getFavoriteList(String userObjectId) {
        BmobQuery<Favorite> query = new BmobQuery<>();
        query.addWhereEqualTo(FIELD_USER_ID, userObjectId);
        query.include(FIELD_MUSIC);
        return query.findObjectsObservable(Favorite.class);
    }

    /**
     * 添加一条收藏
     *
     * @param favorite 收藏内容实体
     * @return
     */
    public static Observable<String> addFavorite(Favorite favorite) {
        return favorite.saveObservable();
    }

    /**
     * 删除一条收藏
     *
     * @param favoriteObjectId 收藏ObjectId
     * @return
     */
    public static Observable<Void> deleteFavorite(String favoriteObjectId) {
        Favorite favorite = new Favorite();
        return favorite.deleteObservable(favoriteObjectId);
    }

    /**
     * 查询用户是否收藏该歌曲
     *
     * @param userObjectId  用户ObjectId
     * @param musicObjectId 歌曲ObjectId
     * @return
     */
    public static Observable<Favorite> isFavorite(String userObjectId, String musicObjectId) {
        BmobQuery<Favorite> query = new BmobQuery<>();
        query.addWhereEqualTo(FIELD_USER_ID, userObjectId);
        query.addWhereEqualTo(FIELD_MUSIC_ID, musicObjectId);
        return query.findObjectsObservable(Favorite.class)
                .map(new Func1<List<Favorite>, Favorite>() {
                    @Override
                    public Favorite call(List<Favorite> favorites) {
                        if (favorites != null && favorites.size() > 0) {
                            return favorites.get(0);
                        }
                        return null;
                    }
                });
    }
}
