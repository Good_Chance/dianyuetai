package com.music.dianyuetai.player;

import android.media.MediaPlayer;
import android.support.annotation.IntDef;
import android.text.TextUtils;
import android.util.Log;

import com.hwangjr.rxbus.RxBus;
import com.kingsoft.media.httpcache.OnCacheStatusListener;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.db.DaoManager;
import com.music.dianyuetai.event.UpdatePlayerEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.music.dianyuetai.download.DownloadManager.COMPLETED;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月13日-09时39分
 */

public class MusicManager {

    /**
     * 播放模式
     */
    public static final int ONE = 0;
    public static final int ORDER = 1;
    public static final int REPEAT_ONE = 2;
    public static final int REPEAT = 3;
    public static final int SHUFFLE = 4;

    @IntDef({ONE, ORDER, REPEAT_ONE, REPEAT, SHUFFLE})
    public @interface Mode {
    }

    /**
     * 播放状态
     */
    public static final int STOP = 0;
    public static final int PLAYING = 1;
    public static final int PAUSE = 2;

    @IntDef({STOP, PLAYING, PAUSE})
    public @interface State {
    }

    private IPlayer player;

    private static MusicManager instance;

    //当前模式
    private int currentMode = REPEAT;
    private int currentState = STOP;
    //当前歌曲索引
    private int currentIndex = -1;
    //当前歌曲
    private LocalMusic currentMusic = null;

    //缓存进度
    private int bufferPosition = 0;

    //歌单
    private List<LocalMusic> playList;

    //本地数据库管理
    private DaoManager daoManager;

    private Timer timer = new Timer();
    private TimerTask timerTask;

    private MusicManager() {
        init();
    }

    public static MusicManager getInstance() {
        if (instance == null) {
            instance = new MusicManager();
        }

        return instance;
    }

    private void init() {
        //初始化歌单
        daoManager = DaoManager.getInstance();
        playList = daoManager.getPlayList();

        if (playList == null) {
            playList = new ArrayList<>();
        }
    }

    public void setPlayer(IPlayer iPlayer) {
        this.player = iPlayer;
        //初始化播放器
        player.setOnPreparedListener(new MusicOnPreparedListener());
        player.setOnCompletionListener(new MusicOnCompletionListener());
        player.setOnErrorListener(new MusicOnErrorListener());
        player.setOnCacheStatusListener(new MusicOnCacheStatusListener());
    }

    /**
     * 销毁播放服务
     */
    public void destory() {
        if (currentState != STOP) {
            stop();
        }

        if (playList != null && playList.size() > 0) {
            for (LocalMusic music : playList) {
                daoManager.updateOrInsertMusic(music);
            }
        }

        player = null;
    }

    /**
     * 添加一首歌到顶部，currentIndex加一
     *
     * @param music
     * @return 是否添加
     */
    public boolean add(LocalMusic music) {
        if (playList == null) {
            playList = new ArrayList<>();
        }

        boolean exist = getIndex(music.getMusicId()) >= 0;

        if (!exist) {
            currentIndex++;
            playList.add(0, music);
            daoManager.updateOrInsertMusic(music);
        }

        return !exist;
    }

    /**
     * 根据索引移除歌曲
     *
     * @param index 歌曲索引
     */
    public void remove(int index) {
        int count = getCount();
        if (count == 0 || index < 0) {
            return;
        }

        LocalMusic music = playList.get(index);

        //移除歌曲
        daoManager.deletePlay(music.getMusicId());
        playList.remove(music);

        if (currentIndex == index) {
            stop();
            stopUpdateUITask();

            if (count == 1) {
                //数量为1，则已清空列表
                currentIndex = -1;
                currentMusic = null;
            } else if (index == count - 1) {
                //删除末尾项，播放第一首
                play(0);
            } else {
                play(index);
            }
        }

        bufferPosition = 0;
    }

    /**
     * 清空歌单
     */
    public void removeAll() {
        if (getCount() == 0) {
            return;
        }

        if (currentState != STOP) {
            stop();
        }

        playList.clear();
        daoManager.deleteAllPlay();

        currentIndex = -1;
        currentMusic = null;

        bufferPosition = 0;
        stopUpdateUITask();
    }

    /**
     * 播放当前歌曲
     *
     * @return
     */
    public int play() {
        return play(currentIndex);
    }

    /**
     * 根据索引播放歌曲
     *
     * @param index 歌曲在歌单中的索引
     * @return 播放状态
     */
    @State
    public int play(int index) {
        if (index < 0 || getCount() == 0) {
            return STOP;
        }

        if (currentMusic == null
                || !TextUtils.equals(playList.get(index).getMusicId(), currentMusic.getMusicId())) {
            //play
            if (currentMusic != null) {
                currentMusic.setPlayState(STOP);
            }

            currentIndex = index;
            currentMusic = playList.get(index);
            currentMusic.setPlayState(PLAYING);
            player.stop();

            sendMusicChangeMessage();

            String musicPath = getPath(currentMusic);
            return play(musicPath);
        } else if (currentState == PLAYING) {
            return pause();
        } else if (currentState == PAUSE) {
            return resume();
        }

        return STOP;
    }

    /**
     * @param music
     * @return
     */
    public int play(LocalMusic music) {
        if (music != null) {
            return play(getIndex(music.getMusicId()));
        }
        return STOP;
    }

    /**
     * 播放上一首
     */
    public void playPrevious() {
        int count = getCount();
        if (count == 0) {
            return;
        }

        int index = currentIndex;

        if (currentMode == SHUFFLE) {
            index = new Random().nextInt(count);
        } else {
            index--;
            if (index == 0) {
                index = count - 1;
            }
        }

        if (currentIndex == index) {
            currentIndex = -1;
            currentMusic = null;
        }

        play(index);
    }

    /**
     * 播放下一首
     */
    public void playNext() {
        int count = getCount();
        if (count == 0) {
            return;
        }

        int index = currentIndex;

        if (currentMode == SHUFFLE) {
            index = new Random().nextInt(count);
        } else {
            index++;
            if (index > count - 1) {
                index = 0;
            }
        }

        if (currentIndex == index) {
            currentIndex = -1;
            currentMusic = null;
        }

        play(index);
    }

    /**
     * 根据URL播放歌曲
     *
     * @param url 歌曲URL
     * @return 播放状态
     */
    @State
    private int play(String url) {
        player.play(url);
        return PLAYING;
    }

    /**
     * @param progress
     */
    public void seekTo(long progress) {
        player.seekTo(progress);
    }

    /**
     * 暂停
     *
     * @return 播放状态
     */
    @State
    public int pause() {
        player.pause();
        setCurrentState(PAUSE);
        stopUpdateUITask();

        return PAUSE;
    }

    /**
     * 重新播放
     *
     * @return 播放状态
     */
    @State
    public int resume() {
        player.resume();
        setCurrentState(PLAYING);
        begainUpdateUITask();

        return PLAYING;
    }

    public int stop() {
        player.stop();
        setCurrentState(STOP);
        bufferPosition = 0;
        return STOP;
    }

    /**
     * 根据歌曲ObjectId获取在歌单中的索引
     *
     * @param musicId 歌曲ObjectId
     * @return 索引
     */
    private int getIndex(String musicId) {
        int count = getCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                if (TextUtils.equals(playList.get(i).getMusicId(), musicId)) {
                    return i;
                }
            }
        }

        return -1;
    }

    /**
     * 获取歌曲url，优先获取本地地址
     *
     * @param music
     * @return
     */
    private String getPath(LocalMusic music) {
        Download download = daoManager.getDownload(music.getMusicId());
        if (download != null && download.getDownloadState() == COMPLETED) {
            //检查是否为本地歌曲
            String localPath = download.getLocalPath();
            File file = new File(localPath);
            if (file.exists()) {
                //校验本地歌曲文件是否存在
                return localPath;
            }
        }

        return music.getLMusicUrl();
    }

    /**
     * 设置当前播放状态
     *
     * @param state 播放状态
     */
    private void setCurrentState(@State int state) {
        this.currentState = state;
    }

    /**
     * 获取当前播放状态
     *
     * @return 播放状态
     */
    @State
    public int getCurrentState() {
        return this.currentState;
    }

    /**
     * 设置当前播放模式
     *
     * @param playMode 播放模式
     */
    public void setCurrentMode(@Mode int playMode) {
        this.currentMode = playMode;
    }

    /**
     * 获取当前播放模式
     *
     * @return 播放模式
     */
    @Mode
    public int getCurrentMode() {
        return currentMode;
    }

    /**
     * 获取歌单中的歌曲数量
     *
     * @return 歌曲数量
     */
    public int getCount() {
        return playList == null ? 0 : playList.size();
    }

    /**
     * 获取当前歌曲索引
     *
     * @return 当前歌曲索引
     */
    public int getCurrentIndex() {
        return this.currentIndex;
    }

    /**
     * 获取当前歌曲
     *
     * @return 当前歌曲实体对象
     */
    public LocalMusic getCurrentMusic() {
        return this.currentMusic;
    }

    /**
     * 获取歌单列表
     *
     * @return 歌曲列表集合
     */
    public List<LocalMusic> getPlayList() {
        return this.playList;
    }

    /**
     * 获取当前播放进度
     *
     * @return 当前歌曲播放进度
     */
    public long getCurrentPosition() {
        return player.getCurrentPosition();
    }

    /**
     * 获取当前歌曲总时长
     *
     * @return 当前歌曲总时长
     */
    public long getDuration() {
        return player.getDuration();
    }

    /**
     * 歌曲异步准备完毕，开始播放，启动更新UI任务
     */
    class MusicOnPreparedListener implements MediaPlayer.OnPreparedListener {

        @Override
        public void onPrepared(MediaPlayer mp) {
            mp.start();
            setCurrentState(PLAYING);
            //歌曲改变，开始更新UI任务
            sendMusicChangeMessage();
            begainUpdateUITask();
        }
    }

    /**
     * 歌曲播放完毕，根据播放模式继续下一步操作
     * 单曲播放： NO操作
     * 列表播放： 播放下一首
     * 单曲循环： 播放同一首歌
     * 列表循环： 播放下一首
     * 随机播放： 随机播放下一首
     */
    class MusicOnCompletionListener implements MediaPlayer.OnCompletionListener {

        @Override
        public void onCompletion(MediaPlayer mp) {
            bufferPosition = 0;
            setCurrentState(STOP);
            stopUpdateUITask();
            switch (currentMode) {
                case ONE:
                    break;
                case ORDER:
                    int orderIndex = currentIndex + 1;
                    if (orderIndex < getCount()) {
                        play(orderIndex);
                    }
                    break;
                case REPEAT_ONE:
                    int indexTemp = currentIndex;
                    currentIndex = -1;
                    currentMusic = null;
                    play(indexTemp);
                    break;
                case REPEAT:
                case SHUFFLE:
                    playNext();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 歌曲播放完毕，根据播放模式继续下一步操作
     * 单曲播放： NO操作
     * 列表播放： 播放下一首
     * 单曲循环： NO操作
     * 列表循环： 播放下一首
     * 随机播放： 随机播放下一首
     */
    class MusicOnErrorListener implements MediaPlayer.OnErrorListener {

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            bufferPosition = 0;
            setCurrentState(STOP);
            stopUpdateUITask();
            switch (currentMode) {
                case ONE:
                    break;
                case ORDER:
                    int orderIndex = currentIndex + 1;
                    if (orderIndex < getCount()) {
                        play(orderIndex);
                    }
                    break;
                case REPEAT_ONE:
                    break;
                case REPEAT:
                case SHUFFLE:
                    playNext();
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    /**
     * 歌曲缓冲进度监听，保存当前缓冲进度
     */
    class MusicOnCacheStatusListener implements OnCacheStatusListener {

        @Override
        public void OnCacheStatus(String s, long l, int i) {
            Log.e("aaa", "String:::" + s + "   Long:::" + l + "   int:::" + i);
            bufferPosition = i;
        }
    }

    /**
     * 更新UI任务，发送更新任务消息
     */
    class UpdateUITask extends TimerTask {

        @Override
        public void run() {
            RxBus.get().post(getUpdateEvent());
        }
    }

    /**
     * 发送歌曲改变消息
     */
    public void sendMusicChangeMessage() {
        RxBus.get().post(new com.music.dianyuetai.event.MusicChangeEvent());
    }

    /**
     * 开始执行更新播放器UI任务
     */
    public void begainUpdateUITask() {
        if (timer == null) {
            timer = new Timer();
        }

        if (timerTask != null) {
            timerTask.cancel();
        }

        timerTask = new UpdateUITask();
        timer.schedule(timerTask, 0, 500);
    }

    /**
     * 停止更新播放器UI任务
     */
    public void stopUpdateUITask() {
        if (timerTask != null) {
            timerTask.cancel();
        }

        RxBus.get().post(getUpdateEvent());
    }

    /**
     * 获取更新播放器UI事件对象
     *
     * @return
     */
    private UpdatePlayerEvent getUpdateEvent() {
        UpdatePlayerEvent event = new UpdatePlayerEvent();
        if (currentMusic != null) {
            event.musicId = currentMusic.getMusicId();
        }

        event.index = getCurrentIndex();
        event.playState = getCurrentState();
        event.bufferPosition = this.bufferPosition;

        if (currentState != STOP) {
            event.currentPosition = getCurrentPosition();
            event.duration = getDuration();
        } else {
            event.currentPosition = 0;
            event.duration = 0;
        }

        return event;
    }
}
