package com.music.dianyuetai.player;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.kingsoft.media.httpcache.OnCacheStatusListener;
import com.music.dianyuetai.App;

import java.io.IOException;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月13日-09时05分
 */

public class MusicPlayerService extends Service implements IPlayer {
    public static boolean isServceRunning = false;
    public static boolean isPlaying = false;

    private MediaPlayer mediaPlayer;

    private KSYProxyService server;

    private MediaPlayer.OnPreparedListener onPreparedListener;
    private MediaPlayer.OnCompletionListener onCompletionListener;
    private MediaPlayer.OnErrorListener onErrorListener;

    private OnCacheStatusListener onCacheStatusListener;

    private final IBinder mBinder = new PlayerBinder();

    private String currentUrl;

    public class PlayerBinder extends Binder {
        public MusicPlayerService getService() {
            return MusicPlayerService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isServceRunning = true;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isServceRunning = false;
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                isPlaying = false;
            }

            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        if (server != null && onCacheStatusListener != null && !TextUtils.isEmpty(currentUrl)) {
            server.unregisterCacheStatusListener(onCacheStatusListener, currentUrl);
        }


        Log.e("player", "播放服务销毁");
    }

    @Override
    public void play(String url) {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }

        try {
            mediaPlayer.reset();
            //使用缓存
            if (server == null) {
                server = App.getProxy(this);
                server.startServer();
            }

            currentUrl = url;

            String cacheUrl = server.getProxyUrl(url);

            mediaPlayer.setDataSource(cacheUrl);
            mediaPlayer.prepareAsync();

            if (onCacheStatusListener != null) {
                server.registerCacheStatusListener(onCacheStatusListener, currentUrl);
            }

            if (onPreparedListener != null) {
                mediaPlayer.setOnPreparedListener(onPreparedListener);
            }

            if (onCompletionListener != null) {
                mediaPlayer.setOnCompletionListener(onCompletionListener);
            }

            if (onErrorListener != null) {
                mediaPlayer.setOnErrorListener(onErrorListener);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    @Override
    public void resume() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    @Override
    public void seekTo(long progress) {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo((int) progress);
        }
    }

    @Override
    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    @Override
    public long getCurrentPosition() {
        return mediaPlayer == null ? 0 : mediaPlayer.getCurrentPosition();
    }

    @Override
    public long getDuration() {
        return mediaPlayer == null ? 0 : mediaPlayer.getDuration();
    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.onPreparedListener = onPreparedListener;
    }

    @Override
    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.onCompletionListener = onCompletionListener;
    }

    @Override
    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.onErrorListener = onErrorListener;
    }

    @Override
    public void setOnCacheStatusListener(OnCacheStatusListener onCacheStatusListener) {
        this.onCacheStatusListener = onCacheStatusListener;
    }
}
