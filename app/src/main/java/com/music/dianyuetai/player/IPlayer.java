package com.music.dianyuetai.player;

import android.media.MediaPlayer;

import com.kingsoft.media.httpcache.OnCacheStatusListener;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月13日-09时44分
 */

public interface IPlayer {

    /**
     * 播放歌曲
     *
     * @param url 歌曲URL地址
     */
    void play(String url);

    /**
     * 暂停播放
     */
    void pause();

    /**
     * 重新播放
     */
    void resume();

    /**
     * 跳转至某时间播放
     *
     * @param progress 跳转到的进度
     */
    void seekTo(long progress);

    /**
     * 停止播放
     */
    void stop();

    /**
     * 获取当前播放进度
     *
     * @return
     */
    long getCurrentPosition();

    /**
     * 获取歌曲总时长
     *
     * @return
     */
    long getDuration();


    /**
     * 设置歌曲准备完成监听
     *
     * @param onPreparedListener
     */
    void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener);

    /**
     * 设置歌曲播放完成监听
     *
     * @param onCompletionListener
     */
    void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener);

    /**
     * 设置歌曲异常监听
     *
     * @param onErrorListener
     */
    void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener);

    /**
     * 设置歌曲缓冲监听
     *
     * @param onBufferingUpdateListener
     */
    void setOnCacheStatusListener(OnCacheStatusListener onCacheStatusListener);
}
