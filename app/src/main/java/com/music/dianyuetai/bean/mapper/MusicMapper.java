package com.music.dianyuetai.bean.mapper;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.bean.MusicResource;
import com.music.dianyuetai.bean.local.LocalMusic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月08日-11时37分
 */

public class MusicMapper {
    public static LocalMusic toLocal(Music music) {
        if (music != null) {
            LocalMusic localMusic = new LocalMusic();
            localMusic.setMusicId(music.getObjectId());
            localMusic.setName(music.getName());
            localMusic.setArtist(music.getArtist());
            localMusic.setPainter(music.getPainter());
            localMusic.setDescription(music.getDescription());
            localMusic.setStyle(music.getStyle());
            localMusic.setDuration(music.getDuration());

            MusicResource resource = music.getResource();

            localMusic.setSPicUrl(resource.getsPicUrl());
            localMusic.setLPicUrl(resource.getlPicUrl());
            localMusic.setLMusicUrl(resource.getlMusicUrl());
            localMusic.setMMusicUrl(resource.getmMusicUrl());
            localMusic.setHMusicUrl(resource.gethMusicUrl());
            localMusic.setCreatedAt(new Date());

            return localMusic;
        }
        return null;
    }

    public static List<LocalMusic> toLocal(List<Music> musicList) {
        if (musicList != null && musicList.size() > 0) {
            List<LocalMusic> localMusicList = new ArrayList<>();
            for (Music music : musicList) {
                LocalMusic localMusic = toLocal(music);
                if (localMusic != null) {
                    localMusicList.add(localMusic);
                }
            }

            return localMusicList;
        }

        return null;
    }
}
