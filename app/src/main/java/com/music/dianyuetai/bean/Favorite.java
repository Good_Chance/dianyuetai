package com.music.dianyuetai.bean;


import cn.bmob.v3.BmobObject;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月18日-16时43分
 */

public class Favorite extends BmobObject {
    //用户id
    private String userId;
    //收藏歌曲
    private Music music;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }
}
