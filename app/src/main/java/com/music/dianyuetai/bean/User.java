package com.music.dianyuetai.bean;


import cn.bmob.v3.BmobUser;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月18日-16时25分
 */
public class User extends BmobUser {
    //用户名称
    private String nickName;
    //用户年龄
    private Integer age;
    //用户性别
    private Boolean gender;
    //头像URL
    private String iconUrl;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
