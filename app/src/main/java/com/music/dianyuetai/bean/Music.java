package com.music.dianyuetai.bean;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobDate;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月18日-16时12分
 */
public class Music extends BmobObject {
    //音乐名称
    private String name;
    //制作人
    private String artist;
    //画师
    private String painter;
    //简介
    private String description;
    //歌曲编号
    private Integer position;
    //风格
    private String style;
    //歌曲总时长
    private Long duration;
    //发布日期
    private BmobDate publishedTime;
    //播放次数
    private Integer playNum;
    //收藏次数
    private Integer favoriteNum;
    //评论数
    private Integer commentNum;
    //歌曲资源
    private MusicResource resource;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPainter() {
        return painter;
    }

    public void setPainter(String painter) {
        this.painter = painter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public BmobDate getPublishedTime() {
        return publishedTime;
    }

    public void setPublishedTime(BmobDate publishedTime) {
        this.publishedTime = publishedTime;
    }

    public Integer getPlayNum() {
        return playNum;
    }

    public void setPlayNum(Integer playNum) {
        this.playNum = playNum;
    }

    public Integer getFavoriteNum() {
        return favoriteNum;
    }

    public void setFavoriteNum(Integer favoriteNum) {
        this.favoriteNum = favoriteNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public MusicResource getResource() {
        return resource;
    }

    public void setResource(MusicResource resource) {
        this.resource = resource;
    }
}
