package com.music.dianyuetai.bean.local;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月08日-10时40分
 */

@Entity
public class LocalMusic {
    @Id(autoincrement = true)
    private Long id;
    @Unique
    private String musicId;
    //音乐名称
    private String name;
    //制作人
    private String artist;
    //画师
    private String painter;
    //简介
    private String description;
    //风格
    private String style;
    //歌曲总时长
    private Long duration;

    //小图
    private String sPicUrl;
    //大图
    private String lPicUrl;

    //低清MP3 Url
    private String lMusicUrl;
    //标清MP3 Url
    private String mMusicUrl;
    //高清MP3 Url
    private String hMusicUrl;

    //播放状态
    private int playState = 0;

    //是否在播放列表
    private boolean isInList;

    //添加列表时间
    private Date createdAt;

    @Generated(hash = 298579123)
    public LocalMusic(Long id, String musicId, String name, String artist,
            String painter, String description, String style, Long duration,
            String sPicUrl, String lPicUrl, String lMusicUrl, String mMusicUrl,
            String hMusicUrl, int playState, boolean isInList, Date createdAt) {
        this.id = id;
        this.musicId = musicId;
        this.name = name;
        this.artist = artist;
        this.painter = painter;
        this.description = description;
        this.style = style;
        this.duration = duration;
        this.sPicUrl = sPicUrl;
        this.lPicUrl = lPicUrl;
        this.lMusicUrl = lMusicUrl;
        this.mMusicUrl = mMusicUrl;
        this.hMusicUrl = hMusicUrl;
        this.playState = playState;
        this.isInList = isInList;
        this.createdAt = createdAt;
    }

    @Generated(hash = 1197306124)
    public LocalMusic() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMusicId() {
        return this.musicId;
    }

    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPainter() {
        return this.painter;
    }

    public void setPainter(String painter) {
        this.painter = painter;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStyle() {
        return this.style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Long getDuration() {
        return this.duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getSPicUrl() {
        return this.sPicUrl;
    }

    public void setSPicUrl(String sPicUrl) {
        this.sPicUrl = sPicUrl;
    }

    public String getLPicUrl() {
        return this.lPicUrl;
    }

    public void setLPicUrl(String lPicUrl) {
        this.lPicUrl = lPicUrl;
    }

    public String getLMusicUrl() {
        return this.lMusicUrl;
    }

    public void setLMusicUrl(String lMusicUrl) {
        this.lMusicUrl = lMusicUrl;
    }

    public String getMMusicUrl() {
        return this.mMusicUrl;
    }

    public void setMMusicUrl(String mMusicUrl) {
        this.mMusicUrl = mMusicUrl;
    }

    public String getHMusicUrl() {
        return this.hMusicUrl;
    }

    public void setHMusicUrl(String hMusicUrl) {
        this.hMusicUrl = hMusicUrl;
    }

    public int getPlayState() {
        return this.playState;
    }

    public void setPlayState(int playState) {
        this.playState = playState;
    }

    public boolean getIsInList() {
        return this.isInList;
    }

    public void setIsInList(boolean isInList) {
        this.isInList = isInList;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
