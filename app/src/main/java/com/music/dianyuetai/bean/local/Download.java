package com.music.dianyuetai.bean.local;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Unique;

import java.util.Date;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import com.music.dianyuetai.db.DaoSession;
import com.music.dianyuetai.db.LocalMusicDao;
import com.music.dianyuetai.db.DownloadDao;
import com.music.dianyuetai.utils.okhttp.request.RequestCall;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月08日-10时49分
 */

@Entity
public class Download {
    @Id(autoincrement = true)
    private Long id;
    //歌曲数据库id
    @Unique
    private Long musicId;
    //歌曲id
    private String musicObjectId;
    //下载的歌曲
    @ToOne(joinProperty = "musicId")
    private LocalMusic music;

    //下载歌曲音质
    private int quality;

    //下载状态
    private int downloadState = 0;

    //文件大小
    private long fileSize;
    //下载进度
    private long downloadProgress;

    //本地地址
    private String localPath;

    //下载任务创建时间
    private Date createdAt;
    //下载完成时间
    private Date completeAt;

    @Transient
    private RequestCall call;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 1547976343)
    private transient DownloadDao myDao;

    @Generated(hash = 995427875)
    public Download(Long id, Long musicId, String musicObjectId, int quality,
            int downloadState, long fileSize, long downloadProgress,
            String localPath, Date createdAt, Date completeAt) {
        this.id = id;
        this.musicId = musicId;
        this.musicObjectId = musicObjectId;
        this.quality = quality;
        this.downloadState = downloadState;
        this.fileSize = fileSize;
        this.downloadProgress = downloadProgress;
        this.localPath = localPath;
        this.createdAt = createdAt;
        this.completeAt = completeAt;
    }

    @Generated(hash = 1462805409)
    public Download() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMusicId() {
        return this.musicId;
    }

    public void setMusicId(Long musicId) {
        this.musicId = musicId;
    }

    public String getMusicObjectId() {
        return this.musicObjectId;
    }

    public void setMusicObjectId(String musicObjectId) {
        this.musicObjectId = musicObjectId;
    }

    public int getQuality() {
        return this.quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getDownloadState() {
        return this.downloadState;
    }

    public void setDownloadState(int downloadState) {
        this.downloadState = downloadState;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getDownloadProgress() {
        return this.downloadProgress;
    }

    public void setDownloadProgress(long downloadProgress) {
        this.downloadProgress = downloadProgress;
    }

    public String getLocalPath() {
        return this.localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getCompleteAt() {
        return this.completeAt;
    }

    public void setCompleteAt(Date completeAt) {
        this.completeAt = completeAt;
    }

    public RequestCall getCall() {
        return call;
    }

    public void setCall(RequestCall call) {
        this.call = call;
    }

    @Generated(hash = 993622651)
    private transient Long music__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1503143178)
    public LocalMusic getMusic() {
        Long __key = this.musicId;
        if (music__resolvedKey == null || !music__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            LocalMusicDao targetDao = daoSession.getLocalMusicDao();
            LocalMusic musicNew = targetDao.load(__key);
            synchronized (this) {
                music = musicNew;
                music__resolvedKey = __key;
            }
        }
        return music;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1396709121)
    public void setMusic(LocalMusic music) {
        synchronized (this) {
            this.music = music;
            musicId = music == null ? null : music.getId();
            music__resolvedKey = musicId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 313503501)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getDownloadDao() : null;
    }
}
