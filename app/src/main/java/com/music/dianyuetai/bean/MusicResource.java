package com.music.dianyuetai.bean;

import cn.bmob.v3.BmobObject;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月08日-10时29分
 */

public class MusicResource extends BmobObject {
    private String name;

    //小图
    private String sPicUrl;
    //大图
    private String lPicUrl;
    //原图
    private String originalPicUrl;

    //低清MP3 Url
    private String lMusicUrl;
    //标清MP3 Url
    private String mMusicUrl;
    //高清MP3 Url
    private String hMusicUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getsPicUrl() {
        return sPicUrl;
    }

    public void setsPicUrl(String sPicUrl) {
        this.sPicUrl = sPicUrl;
    }

    public String getlPicUrl() {
        return lPicUrl;
    }

    public void setlPicUrl(String lPicUrl) {
        this.lPicUrl = lPicUrl;
    }

    public String getOriginalPicUrl() {
        return originalPicUrl;
    }

    public void setOriginalPicUrl(String originalPicUrl) {
        this.originalPicUrl = originalPicUrl;
    }

    public String getlMusicUrl() {
        return lMusicUrl;
    }

    public void setlMusicUrl(String lMusicUrl) {
        this.lMusicUrl = lMusicUrl;
    }

    public String getmMusicUrl() {
        return mMusicUrl;
    }

    public void setmMusicUrl(String mMusicUrl) {
        this.mMusicUrl = mMusicUrl;
    }

    public String gethMusicUrl() {
        return hMusicUrl;
    }

    public void sethMusicUrl(String hMusicUrl) {
        this.hMusicUrl = hMusicUrl;
    }
}
