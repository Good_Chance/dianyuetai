package com.music.dianyuetai.bean;

import cn.bmob.v3.BmobObject;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月18日-16时24分
 */
public class Comment extends BmobObject{
    //音乐id
    private String musicId;
    //评论人
    private User user;
    //评论内容
    private String content;
    //点赞数
    private Integer support;

    public String getMusicId() {
        return musicId;
    }

    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSupport() {
        return support;
    }

    public void setSupport(Integer support) {
        this.support = support;
    }
}