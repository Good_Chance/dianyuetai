package com.music.dianyuetai.bean;


import cn.bmob.v3.BmobObject;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-15时08分
 */

public class Recommend extends BmobObject{
    //推荐歌曲
    private Music music;

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }
}
