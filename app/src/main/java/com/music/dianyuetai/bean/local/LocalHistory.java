package com.music.dianyuetai.bean.local;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;

import java.util.Date;

import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

import com.music.dianyuetai.db.DaoSession;
import com.music.dianyuetai.db.LocalMusicDao;
import com.music.dianyuetai.db.LocalHistoryDao;

import org.greenrobot.greendao.annotation.NotNull;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月08日-10时49分
 */
@Entity
public class LocalHistory {
    @Id(autoincrement = true)
    private Long id;
    //歌曲数据库id
    private long musicId;
    //歌曲id
    @Unique
    private String musicObjectId;
    //播放歌曲
    @ToOne(joinProperty = "musicId")
    private LocalMusic music;
    //最后播放时间
    private Date updateAt;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /**
     * Used for active entity operations.
     */
    @Generated(hash = 428167158)
    private transient LocalHistoryDao myDao;

    @Generated(hash = 416503422)
    public LocalHistory(Long id, long musicId, String musicObjectId, Date updateAt) {
        this.id = id;
        this.musicId = musicId;
        this.musicObjectId = musicObjectId;
        this.updateAt = updateAt;
    }

    @Generated(hash = 2031124405)
    public LocalHistory() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getMusicId() {
        return this.musicId;
    }

    public void setMusicId(long musicId) {
        this.musicId = musicId;
    }

    public String getMusicObjectId() {
        return this.musicObjectId;
    }

    public void setMusicObjectId(String musicObjectId) {
        this.musicObjectId = musicObjectId;
    }

    public Date getUpdateAt() {
        return this.updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Generated(hash = 993622651)
    private transient Long music__resolvedKey;

    /**
     * To-one relationship, resolved on first access.
     */
    @Generated(hash = 2058653984)
    public LocalMusic getMusic() {
        long __key = this.musicId;
        if (music__resolvedKey == null || !music__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            LocalMusicDao targetDao = daoSession.getLocalMusicDao();
            LocalMusic musicNew = targetDao.load(__key);
            synchronized (this) {
                music = musicNew;
                music__resolvedKey = __key;
            }
        }
        return music;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 1113429012)
    public void setMusic(@NotNull LocalMusic music) {
        if (music == null) {
            throw new DaoException(
                    "To-one property 'musicId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.music = music;
            musicId = music.getId();
            music__resolvedKey = musicId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 621584218)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getLocalHistoryDao() : null;
    }

}
