package com.music.dianyuetai.download;

import android.os.Environment;
import android.support.annotation.IntDef;

import com.hwangjr.rxbus.RxBus;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.db.DaoManager;
import com.music.dianyuetai.event.UpdateDownloadEvent;
import com.music.dianyuetai.utils.okhttp.OkHttpUtils;
import com.music.dianyuetai.utils.okhttp.callback.FileCallBack;
import com.music.dianyuetai.utils.okhttp.request.RequestCall;

import java.io.File;
import java.util.List;

import okhttp3.Call;

/**
 * Created by Tang on 2016/4/28 0028.
 */
public class DownloadManager {
    public static final int FAILURE = -1;
    public static final int LOADING = 1;
    public static final int PAUSE = 2;
    public static final int COMPLETED = 3;

    @IntDef({FAILURE, LOADING, PAUSE, COMPLETED})
    public @interface State {
    }


    private DaoManager daoManager;

    private List<Download> taskList;

    private List<Download> completeList;

    public DownloadManager() {
        daoManager = DaoManager.getInstance();
        taskList = daoManager.getDownloadingList();
        completeList = daoManager.getDownloadedList();
    }

    public int getDownloadingCount() {
        return taskList.size();
    }

    public int getCompletedCount() {
        return completeList.size();
    }

    public List<Download> getTaskList() {
        return this.taskList;
    }

    public List<Download> getCompleteList() {
        return this.completeList;
    }

    public synchronized void addNewTask(Download download) {
        if (download != null) {
            RequestCall call = OkHttpUtils
                    .get()
                    .url(download.getMusic().getLMusicUrl())
                    .build();

            download.setCall(call);
            taskList.add(0, download);
            daoManager.updateOrInsertDownload(download);

            call.execute(new DownloadCallBack(download));
        }
    }

    public String getDownloadPath() {
        String path = Environment.getExternalStorageDirectory() + "/dianyuetai";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }
        return path;
    }

    /**
     * 继续下载
     *
     * @param index
     */
    public synchronized void resumeTask(int index) {
        Download download = taskList.get(index);
        RequestCall call = OkHttpUtils
                .get()
                .addHeader("RANGE", "bytes=" + download.getDownloadProgress() + "-" + download.getFileSize())
                .url(download.getMusic().getLMusicUrl())
                .build();

        download.setCall(call);
        call.execute(new DownloadCallBack(download));
        daoManager.updateOrInsertDownload(download);
    }

    /**
     * 继续下载任务
     *
     * @param download
     */
    public void resumeTask(Download download) {
        if (download != null) {
            int index = taskList.indexOf(download);
            if (index >= 0) {
                resumeTask(index);
            }
        }
    }

    /**
     * 暂停任务
     *
     * @param index 任务索引
     */
    public void pauseTask(int index) {
        Download download = taskList.get(index);
        RequestCall call = download.getCall();
        if (call != null) {
            call.cancel();
        }
        setTaskState(download, PAUSE);
    }

    /**
     * 移除下载任务
     *
     * @param index
     */
    public void removeTask(int index) {
        Download download = taskList.get(index);
        RequestCall call = download.getCall();
        if (call != null) {
            call.cancel();
        }

        taskList.remove(index);
        daoManager.deleteDownload(download);
    }

    public void removeAllTask() {
        taskList.clear();
    }

    public void stopAllTask() {
        for (Download download : taskList) {
            RequestCall call = download.getCall();
            if (call != null) {
                call.cancel();
            }

            if (download.getDownloadState() == LOADING) {
                setTaskState(download, PAUSE);
            }
        }
    }

    public synchronized void backupAllTask() {
        for (Download download : taskList) {
            daoManager.updateOrInsertDownload(download);
        }

        for (Download download : completeList) {
            daoManager.updateOrInsertDownload(download);
        }
    }

    public void setTaskState(Download download, @State int state) {
        if (download != null) {
            download.setDownloadState(state);
        }
    }

    public class DownloadCallBack extends FileCallBack {
        long start = 0;

        Download download;

        UpdateDownloadEvent event = null;

        public DownloadCallBack(Download download) {
            super(getDownloadPath(), download.getMusic().getName() + ".mp3", download.getDownloadProgress());
            this.event = new UpdateDownloadEvent();
            this.download = download;
            download.setLocalPath(getDownloadPath() + "/" + download.getMusic().getName() + ".mp3");
        }

        @Override
        public void inProgress(float progress, long total, int id) {
            long currentTime = System.currentTimeMillis();

            if (currentTime - start >= 500 && progress < 1) {
                //正在下载
                start = currentTime;

                download.setDownloadState(LOADING);
                download.setFileSize(total);
                download.setDownloadProgress((long) (progress * total));

                RxBus.get().post(event);
            } else if (progress == 1) {
                //下载完成
                download.setDownloadState(COMPLETED);
                download.setFileSize(total);
                download.setDownloadProgress(total);

                completeList.add(0, download);
                taskList.remove(download);

                RxBus.get().post(event);
            }
        }

        @Override
        public void onError(Call call, Exception e, int id) {
            call.cancel();
            e.printStackTrace();
            //下载异常

            download.setDownloadProgress(0);
            download.setDownloadState(FAILURE);

            RxBus.get().post(event);
        }

        @Override
        public void onResponse(File response, int id) {

        }
    }
}