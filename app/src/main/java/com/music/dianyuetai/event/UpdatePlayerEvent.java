package com.music.dianyuetai.event;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月13日-13时35分
 */

public class UpdatePlayerEvent {
    public int index;
    public String musicId;

    public long currentPosition;
    public long duration;

    public int bufferPosition;

    public int playState;
}
