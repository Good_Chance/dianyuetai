package com.music.dianyuetai.event;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月14日-11时25分
 */

public class UpdateDownloadEvent {
    public int index;
    public long fileSize;
    public long downloadProgress;
    public int downloadState;
}
