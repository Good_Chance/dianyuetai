package com.music.dianyuetai.presenter.impl;

import android.util.Log;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.interactor.IMusicInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IMusicPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.view.MusicsView;

import java.util.List;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月25日-14时42分
 */

public class MusicPresenter implements IMusicPresenter {
    private static final String TAG = "MusicPresenter";

    private MusicsView view;
    private IMusicInteractor interactor;

    private SubscriptionManager manager;

    private int currentPage = 1;

    public MusicPresenter(MusicsView view, IMusicInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.manager = SubscriptionManager.getInstance();
    }

    @Override
    public void initializes() {
        refresh();
    }

    @Override
    public void refresh() {
        currentPage = 1;
        loadMore();
    }

    @Override
    public void loadMore() {
        manager.addSubscription(
                interactor.getMusicList(currentPage, "name", new MusicSubscriber())
        );
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        manager.removeAllSubscription();
    }

    private class MusicSubscriber extends BaseSubscriber<List<Music>> {


        @Override
        public void onCompleted() {
            currentPage++;
        }

        @Override
        public void onError(Throwable throwable) {
            Log.e(TAG, throwable.getMessage());
            if (currentPage == 1) {
                view.setRefreshState(LoadState.FAILURE);
            } else {
                view.setLoadState(LoadState.FAILURE);
            }
        }

        @Override
        public void onNext(List<Music> musics) {
            Log.e(TAG, "musicSize:::" + (musics == null ? 0 : musics.size()));
            if (musics != null && musics.size() > 0) {
                if (currentPage == 1) {
                    view.refresh(musics);
                    view.setRefreshState(LoadState.COMPLETE);
                } else {
                    view.loadMore(musics);
                    if (musics.size() < 10) {
                        view.setLoadState(LoadState.NOMORE);
                    } else {
                        view.setLoadState(LoadState.COMPLETE);
                    }
                }
            } else {
                if (currentPage == 1) {
                    view.setRefreshState(LoadState.NOMORE);
                } else {
                    view.setLoadState(LoadState.NOMORE);
                }
            }
        }
    }
}
