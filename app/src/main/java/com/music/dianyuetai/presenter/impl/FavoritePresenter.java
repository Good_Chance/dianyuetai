package com.music.dianyuetai.presenter.impl;

import com.music.dianyuetai.App;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.interactor.IFavoriteInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IFavoritePresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.view.FavoriteView;

import java.util.List;

import rx.Subscription;

/**
 * Created by tang on 2016/10/19 0019.
 */

public class FavoritePresenter implements IFavoritePresenter{
    private FavoriteView view;
    private IFavoriteInteractor interactor;

    private SubscriptionManager subscriptionManager;

    public FavoritePresenter(FavoriteView view, IFavoriteInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.subscriptionManager= SubscriptionManager.getInstance();
    }

    @Override
    public void initializes() {
        User user = App.getAppContext().getUser();
        if(user!=null){
            subscriptionManager.addSubscription(
                    interactor.getFavoriteList(user.getObjectId(),new FavoriteSubscriber())
            );
        }

    }

    @Override
    public void delete(Favorite favorite) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    private class FavoriteSubscriber extends BaseSubscriber<List<Favorite>>{

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {

        }

        @Override
        public void onNext(List<Favorite> favorites) {
            view.refresh(favorites);
        }
    }
}
