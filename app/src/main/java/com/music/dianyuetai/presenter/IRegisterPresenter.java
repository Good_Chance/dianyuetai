package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-16时23分
 */

public interface IRegisterPresenter extends BasePresenter{
    /**
     * 注册账号
     */
    void register();
}
