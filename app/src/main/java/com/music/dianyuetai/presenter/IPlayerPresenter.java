package com.music.dianyuetai.presenter;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月31日-10时58分
 */

public interface IPlayerPresenter extends BasePresenter {
    /**
     * 初始化播放页数据
     */
    void initializes();

    /**
     * 播放/暂停
     */
    void play();

    /**
     * 加载评论
     */
    void loadComment();

    /**
     * 发送评论
     */
    void sendComment();

    /**
     * 添加收藏/取消收藏
     */
    void favorite();

    /**
     * 添加下载任务
     */
    void download();

    /**
     * 设置播放时间
     *
     * @param i
     */
    void seekTo(long i);
}
