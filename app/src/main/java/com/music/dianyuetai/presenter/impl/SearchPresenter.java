package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;
import android.util.Log;

import com.music.dianyuetai.api.MusicApi;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.interactor.ISearchInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.ISearchPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.view.SearchView;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-13时16分
 */

public class SearchPresenter implements ISearchPresenter {
    private SearchView view;
    private ISearchInteractor interactor;

    private SubscriptionManager manager;

    private int currentPage = 1;

    private String selection;

    public SearchPresenter(SearchView view, ISearchInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.manager = SubscriptionManager.getInstance();
    }

    @Override
    public void search() {
        selection = view.getSelection();
        Log.e("aaaa", "查询条件：：：" + selection);
        if (!TextUtils.isEmpty(selection)) {
            currentPage = 1;
            manager.addSubscription(
                    interactor.getSearchList(currentPage, selection, new SearchSubscriber())
            );
        }
    }

    @Override
    public void loadMore() {
        if (!TextUtils.isEmpty(selection)) {
            manager.addSubscription(
                    interactor.getSearchList(currentPage, selection, new SearchSubscriber())
            );
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    private class SearchSubscriber extends BaseSubscriber<List<Music>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            view.setLoadState(LoadState.FAILURE);
        }

        @Override
        public void onNext(List<Music> musicList) {
            Log.e("sfdsdf", "查询结果：：：" + musicList.size());
            if (musicList != null && musicList.size() > 0) {
                if (currentPage == 1) {
                    view.refresh(musicList);
                    view.setRefreshState(LoadState.COMPLETE);
                } else {
                    view.loadMore(musicList);
                    if (musicList.size() < 10) {
                        view.setLoadState(LoadState.NOMORE);
                    } else {
                        view.setLoadState(LoadState.COMPLETE);
                    }
                }
            } else {
                if (currentPage == 1) {
                    view.setRefreshState(LoadState.NOMORE);
                } else {
                    view.setLoadState(LoadState.NOMORE);
                }
            }
        }
    }
}
