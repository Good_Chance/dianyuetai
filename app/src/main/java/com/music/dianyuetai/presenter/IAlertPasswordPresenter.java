package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月21日-10时03分
 */

public interface IAlertPasswordPresenter extends BasePresenter{
    void alertPassword();
}
