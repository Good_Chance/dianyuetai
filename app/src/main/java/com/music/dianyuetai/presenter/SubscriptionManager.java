package com.music.dianyuetai.presenter;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * 描述：rx订阅事件管理类。
 * 作者：唐志远
 * 日期：2016年07月26日-13时15分
 */
public class SubscriptionManager {

    private List<Subscription> subscriptionList;

    private SubscriptionManager() {
        //初始化事件订阅集合
        subscriptionList = new ArrayList<>();
    }

    /**
     * 获取SubscriptionManager实例
     *
     * @return
     */
    public static SubscriptionManager getInstance() {
        return new SubscriptionManager();
    }

    /**
     * 添加rx订阅事件
     *
     * @param subscription
     */
    public void addSubscription(Subscription subscription) {
        if (subscriptionList == null) {
            subscriptionList = new ArrayList<>();
        }

        if (subscription != null) {
            subscriptionList.add(subscription);
        }
    }

    /**
     * 移除所有订阅事件
     */
    public void removeAllSubscription() {
        //销毁时取消事件订阅
        if (subscriptionList != null && subscriptionList.size() > 0) {
            int count = subscriptionList.size();

            for (int i = 0; i < count; i++) {
                Subscription subscription = subscriptionList.get(i);
                if (subscription != null && !subscription.isUnsubscribed()) {
                    subscription.unsubscribe();
                }
            }
        }
    }
}
