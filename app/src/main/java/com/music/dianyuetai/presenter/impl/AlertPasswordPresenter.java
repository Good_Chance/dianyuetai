package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;

import com.music.dianyuetai.interactor.IAlertPasswordInteractor;
import com.music.dianyuetai.presenter.IAlertPasswordPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.utils.StringUtils;
import com.music.dianyuetai.view.AlertPasswordView;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月21日-10时03分
 */

public class AlertPasswordPresenter implements IAlertPasswordPresenter {

    private AlertPasswordView view;
    private IAlertPasswordInteractor interactor;
    private SubscriptionManager subscriptionManager;

    public AlertPasswordPresenter(AlertPasswordView view, IAlertPasswordInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.subscriptionManager = SubscriptionManager.getInstance();
    }

    @Override
    public void alertPassword() {
        String oldPassword = view.getOldPassword();
        String newPassword = view.getNewPassword();
        String repeatPassword = view.getRepeatPassword();

        if (TextUtils.isEmpty(oldPassword)) {
            view.toast("原密码不能为空");
            return;
        }

        if (TextUtils.isEmpty(newPassword)) {
            view.toast("新密码不能为空");
            return;
        }
        if (TextUtils.isEmpty(repeatPassword)) {
            view.toast("确认密码不能为空");
            return;
        }

        if (TextUtils.equals(newPassword, repeatPassword)) {
            view.toast("两次密码不一致，请重新输入");
            return;
        }

        //密码进行md5加密
        oldPassword = StringUtils.md5(oldPassword);
        newPassword = StringUtils.md5(newPassword);

        //修改密码
        subscriptionManager.addSubscription(
                interactor.alertPassword(oldPassword, newPassword, new AlertPasswordLinstener())
        );
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        subscriptionManager.removeAllSubscription();
    }

    private class AlertPasswordLinstener extends UpdateListener {

        @Override
        public void done(BmobException e) {
            if (e == null) {
                view.toast("密码修改成功，可以用新密码进行登录啦");
            } else {
                view.toast("失败:" + e.getMessage());
            }
        }
    }
}
