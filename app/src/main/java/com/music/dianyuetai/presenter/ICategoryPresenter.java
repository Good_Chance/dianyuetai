package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时21分
 */

public interface ICategoryPresenter extends BasePresenter {
    /**
     * 初始化分类数据
     */
    void initializes();

    /**
     * 刷新分类数据
     */
    void refresh();
}
