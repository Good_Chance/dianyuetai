package com.music.dianyuetai.presenter;

/**
 * 描述：音乐列表页Presenter接口
 * 作者：唐志远
 * 日期：2016年08月25日-14时41分
 */

public interface IMusicPresenter extends BasePresenter {

    /**
     * 初始化列表数据
     */
    void initializes();

    /**
     * 刷新数据
     */
    void refresh();

    /**
     * 加载更多
     */
    void loadMore();
}
