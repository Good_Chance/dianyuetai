package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月19日-11时24分
 */

public interface IMainPresenter extends BasePresenter{
    /**
     * 初始化主页数据
     */
    void initializes();
}
