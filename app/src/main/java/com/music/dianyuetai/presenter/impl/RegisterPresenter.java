package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;
import android.util.Log;

import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.interactor.IRegisterInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IRegisterPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.utils.StringUtils;
import com.music.dianyuetai.utils.ToastUtils;
import com.music.dianyuetai.view.RegisterView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-16时24分
 */

public class RegisterPresenter implements IRegisterPresenter {

    private RegisterView view;
    private IRegisterInteractor interactor;

    private SubscriptionManager manager;

    public RegisterPresenter(RegisterView view, IRegisterInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.manager = SubscriptionManager.getInstance();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void register() {
        String account = view.getAccount();
        String nickName = view.getNickName();
        String password = view.getPassword();

        if (TextUtils.isEmpty(account)) {
            ToastUtils.show("账号不能为空");
            return;
        }
        if (TextUtils.isEmpty(nickName)) {
            ToastUtils.show("昵称不能为空");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("密码不能为空");
            return;
        }

        //密码进行md5加密
        password = StringUtils.md5(password);
        Log.e("eeeee", password);
        //注册
        manager.addSubscription(
                interactor.register(account, nickName, password, new RegisterSubscriber())
        );
    }

    private class RegisterSubscriber extends BaseSubscriber<User> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
            ToastUtils.show("注册失败！");
        }

        @Override
        public void onNext(User user) {
            ToastUtils.show("注册成功！");
        }
    }
}
