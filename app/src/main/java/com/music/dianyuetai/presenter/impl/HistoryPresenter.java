package com.music.dianyuetai.presenter.impl;

import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.interactor.IHistoryInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IHistoryPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.view.HistoryView;

import java.util.List;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月22日-10时51分
 */

public class HistoryPresenter implements IHistoryPresenter {
    private HistoryView view;
    private IHistoryInteractor interactor;

    private SubscriptionManager manager;

    public HistoryPresenter(HistoryView view, IHistoryInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.manager = SubscriptionManager.getInstance();
    }

    @Override
    public void initializes() {
        manager.addSubscription(
                interactor.getHistoryList(new HistoryObsever())
        );
    }

    @Override
    public void deleteAll() {
        interactor.deleteAllHistory();
        view.deleteAll();
        view.setRefreshState(LoadState.NOMORE);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        manager.removeAllSubscription();
    }

    private class HistoryObsever extends BaseSubscriber<List<LocalHistory>> {

        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }

        @Override
        public void onNext(List<LocalHistory> localHistories) {
            if (localHistories != null && localHistories.size() > 0) {
                view.refresh(localHistories);
                view.setRefreshState(LoadState.COMPLETE);
            } else {
                view.setRefreshState(LoadState.NOMORE);
            }
        }
    }
}
