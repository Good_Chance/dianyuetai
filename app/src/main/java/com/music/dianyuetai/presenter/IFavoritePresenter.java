package com.music.dianyuetai.presenter;

import com.music.dianyuetai.bean.Favorite;

/**
 * Created by tang on 2016/10/19 0019.
 */

public interface IFavoritePresenter extends BasePresenter {
    /**
     * 初始化收藏揶数据
     */
    void initializes();

    /**
     * 删除收藏
     *
     * @param favorite
     */
    void delete(Favorite favorite);
}
