package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-14时37分
 */

public interface ILoginPresenter extends BasePresenter {
    /**
     * 登录
     */
    void login();
}
