package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-14时20分
 */

public interface IRecommendPresenter extends BasePresenter {
    /**
     * 初始化推荐页数据
     */
    void initializes();

    /**
     * 刷新推荐页数据
     */
    void refresh();
}
