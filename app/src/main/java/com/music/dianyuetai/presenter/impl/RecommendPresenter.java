package com.music.dianyuetai.presenter.impl;

import android.util.Log;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.interactor.IRecommendInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IRecommendPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.view.RecommendView;

import java.util.List;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-14时20分
 */

public class RecommendPresenter implements IRecommendPresenter {
    private static final String TAG = "RecommendPresenter";

    private RecommendView view;
    private IRecommendInteractor interactor;
    private SubscriptionManager manager;

    public RecommendPresenter(RecommendView view, IRecommendInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.manager = SubscriptionManager.getInstance();
    }

    @Override
    public void initializes() {
        refresh();
    }

    @Override
    public void refresh() {
        manager.addSubscription(
                interactor.getMusicList(new RecommnedObsever())
        );
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        manager.removeAllSubscription();
    }

    private class RecommnedObsever extends BaseSubscriber<List<Music>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {
            Log.e(TAG, throwable.getMessage());
            view.setRefreshState(LoadState.FAILURE);
        }

        @Override
        public void onNext(List<Music> musics) {
            if (musics != null && musics.size() > 0) {
                view.refresh(musics);
                view.setRefreshState(LoadState.COMPLETE);
            } else {
                view.setRefreshState(LoadState.NOMORE);
            }
        }
    }
}
