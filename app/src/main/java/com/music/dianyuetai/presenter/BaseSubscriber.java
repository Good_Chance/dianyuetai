/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.presenter;

import rx.Subscriber;

/**
 * 描述：model基类数据访问回调接口
 * 作者：唐志远
 * 日期：2016年06月26日-15时21分
 */
public abstract class BaseSubscriber<T> extends Subscriber<T> {
}
