package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-13时16分
 */

public interface ISearchPresenter extends BasePresenter {
    /**
     * 搜索
     */
    void search();

    /**
     * 加载更多搜索结果
     */
    void loadMore();
}
