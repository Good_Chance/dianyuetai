package com.music.dianyuetai.presenter.impl;

import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.interactor.ICategoryInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.ICategoryPresenter;
import com.music.dianyuetai.view.CategoryView;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时22分
 */

public class CategoryPresenter implements ICategoryPresenter {

    private CategoryView view;
    private ICategoryInteractor interactor;

    public CategoryPresenter(CategoryView view, ICategoryInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void initializes() {
        refresh();
    }

    @Override
    public void refresh() {
        interactor.getCategoryList(new CategorySubscriber());
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    private class CategorySubscriber extends BaseSubscriber<List<List<String>>> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {

        }

        @Override
        public void onNext(List<List<String>> lists) {
            view.refresh(lists);
            view.setRefreshState(LoadState.COMPLETE);
        }
    }
}
