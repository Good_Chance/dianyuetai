package com.music.dianyuetai.presenter;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月22日-10时50分
 */

public interface IHistoryPresenter extends BasePresenter{
    /**
     * 初始化历史记录数据
     */
    void initializes();

    /**
     * 删除所有历史记录
     */
    void deleteAll();
}
