/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.presenter;

/**
 * 描述：presenter抽象类基类。
 * 作者：唐志远
 * 日期：2016年07月20日-09时40分
 */
public interface BasePresenter {

    /**
     * 在重启时进行的操作
     */
    void onResume();

    /**
     * 在销毁时进行的操作
     */
    void onDestroy();
}
