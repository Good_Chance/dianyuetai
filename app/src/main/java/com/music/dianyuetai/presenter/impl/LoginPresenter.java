package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;
import android.util.Log;

import com.hwangjr.rxbus.RxBus;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.event.LoginEvent;
import com.music.dianyuetai.interactor.ILoginInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.ILoginPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.utils.StringUtils;
import com.music.dianyuetai.utils.ToastUtils;
import com.music.dianyuetai.view.LoginView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-14时41分
 */

public class LoginPresenter implements ILoginPresenter {

    private LoginView view;
    private ILoginInteractor interactor;

    private SubscriptionManager subscriptionManager;

    public LoginPresenter(LoginView view, ILoginInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.subscriptionManager = SubscriptionManager.getInstance();
    }

    @Override
    public void login() {
        String account = view.getAccount();
        String password = view.getPassword();

        if (TextUtils.isEmpty(account)) {
            ToastUtils.show("请输入账号");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("请输入密码");
            return;
        }

        //密码进行md5加密
        password = StringUtils.md5(password);
        Log.e("eeeee", "login:::" + password);
        //登录
        subscriptionManager.addSubscription(
                interactor.login(account, password, new LoginSubscriber())
        );
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        subscriptionManager.removeAllSubscription();
    }

    private class LoginSubscriber extends BaseSubscriber<User> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
            ToastUtils.show("登录失败");
        }

        @Override
        public void onNext(User user) {
            if (user != null) {
                ToastUtils.show("登录成功！");
                view.finish();
                RxBus.get().post(new LoginEvent());
            } else {
                ToastUtils.show("登录失败");
            }
        }
    }
}
