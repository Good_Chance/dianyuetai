package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;
import android.util.Log;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.music.dianyuetai.bean.Comment;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.bean.mapper.MusicMapper;
import com.music.dianyuetai.constant.LoadState;
import com.music.dianyuetai.db.DaoManager;
import com.music.dianyuetai.download.DownloadManager;
import com.music.dianyuetai.download.DownloadService;
import com.music.dianyuetai.event.UpdatePlayerEvent;
import com.music.dianyuetai.interactor.IPlayerInteractor;
import com.music.dianyuetai.player.MusicManager;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IPlayerPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.utils.ToastUtils;
import com.music.dianyuetai.view.MusicPlayerView;

import java.util.Date;
import java.util.List;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobDate;

import static com.music.dianyuetai.player.MusicManager.PLAYING;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月31日-11时14分
 */

public class PlayerPresenter implements IPlayerPresenter {
    private static final String TAG = "PlayerPresenter";

    private MusicPlayerView view;
    private IPlayerInteractor interactor;

    private SubscriptionManager manager;

    private String musicId;

    private int currentCommentPage = 1;

    private Music music;
    private LocalMusic localMusic;

    private MusicManager musicManager;

    private DownloadManager downloadManager;

    public PlayerPresenter(MusicPlayerView view, IPlayerInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.musicManager = MusicManager.getInstance();
        this.manager = SubscriptionManager.getInstance();
        this.downloadManager = DownloadService.getDownloadManager(view.getContext());
    }

    @Override
    public void initializes() {
        musicId = view.getMusicId();
        manager.addSubscription(
                interactor.getMusicDetail(musicId, new MusicDetailSubscriber())
        );
        loadComment();
        RxBus.get().register(this);
    }

    @Override
    public void play() {
        if (music != null) {
            LocalMusic localMusic = MusicMapper.toLocal(music);
            localMusic.setIsInList(true);
            musicManager.add(localMusic);
            //播放/暂停
            int state = musicManager.play(localMusic);
            if (state == PLAYING) {
                view.hidePlayerPanel();
                view.setPlayBtnState(true);
                Log.e("asdfsdfsd", "隐藏时间栏");
            } else {
                view.showPlayerPanel();
                view.setPlayBtnState(false);
            }
        }
    }

    @Override
    public void loadComment() {
        manager.addSubscription(
                interactor.getComments(musicId, currentCommentPage, new CommentSubscriber())
        );
    }

    @Override
    public void sendComment() {

    }

    @Override
    public void favorite() {
        BmobUser user = BmobUser.getCurrentUser();
        if (user != null) {
            if (music != null) {
                Favorite favorite = new Favorite();
                favorite.setMusic(music);
                favorite.setUserId(user.getObjectId());

                manager.addSubscription(
                        interactor.addFavorite(favorite, new FavoriteSubscriber())
                );
            }
        } else {
            //TODO 跳转到登录页面
        }
    }

    @Override
    public void download() {
        Download download = DaoManager.getInstance().getDownload(musicId);
        if (download == null && localMusic != null) {
            download = new Download();
            download.setMusic(localMusic);
            download.setCreatedAt(new Date());
            download.setMusicId(localMusic.getId());
            download.setMusicObjectId(musicId);

            downloadManager.addNewTask(download);
            ToastUtils.show("开始下载。。。");
        } else {
            ToastUtils.show("已添加下载");
        }
    }

    @Override
    public void seekTo(long i) {
        musicManager.seekTo(i);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void updatePlayerEvent(UpdatePlayerEvent event) {
        if (TextUtils.equals(event.musicId, musicId)) {
            long position = event.currentPosition;
            long duration = event.duration;
            long bufferPosition = (duration * event.bufferPosition) / 100;

            view.setProgress(duration, position);
            view.setBufferProgress(bufferPosition);
            view.setPlayBtnState(event.playState == PLAYING);
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        manager.removeAllSubscription();
        RxBus.get().unregister(this);
    }


    private class MusicDetailSubscriber extends BaseSubscriber<Music> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {
            Log.e("eeee", throwable.getMessage());
        }

        @Override
        public void onNext(Music music) {
            PlayerPresenter.this.music = music;

            //显示歌曲详细
            view.setPreviewImage(music.getResource().getlPicUrl());
            view.setArtist(music.getArtist());
            view.setPainer(music.getPainter());
            view.setMusicName(music.getName());

            PlayerPresenter.this.localMusic = MusicMapper.toLocal(music);
            localMusic.setCreatedAt(new Date());

            long id = interactor.addLocalMusic(localMusic);
            Log.e("musicId", "musicId：：：" + id);
            localMusic.setId(id);

            //添加历史
            LocalHistory localHistory = new LocalHistory();
            localHistory.setMusicObjectId(music.getObjectId());
            localHistory.setMusicId(id);
            localHistory.setUpdateAt(new Date());
            localHistory.setMusic(localMusic);
            interactor.addHistory(localHistory);
        }
    }

    private class CommentSubscriber extends BaseSubscriber<List<Comment>> {

        @Override
        public void onCompleted() {
            currentCommentPage++;
        }

        @Override
        public void onError(Throwable throwable) {
            Log.e(TAG, throwable.getMessage());
            view.setLoadState(LoadState.FAILURE);
        }

        @Override
        public void onNext(List<Comment> comments) {
            Log.e(TAG, "评论数量：：" + (comments == null ? 11111 : comments.size()));
            if (comments != null) {
                if (currentCommentPage == 1) {
                    int refreshState = 0;
                    if (comments.size() == 0) {
                        refreshState = LoadState.NOMORE;
                        view.setAppBarDisable();
                    } else {
                        refreshState = LoadState.COMPLETE;
                    }
                    view.setRefreshState(refreshState);
                } else {
                    view.setLoadState(comments.size() == 0 ? LoadState.NOMORE : LoadState.COMPLETE);
                }
                view.loadComment(comments);
            }
        }
    }

    private class FavoriteSubscriber extends BaseSubscriber<Boolean> {

        @Override
        public void onNext(Boolean value) {
            view.setFavoriteState(value);
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }
    }
}
