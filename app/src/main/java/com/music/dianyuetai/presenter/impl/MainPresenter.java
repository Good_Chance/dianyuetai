package com.music.dianyuetai.presenter.impl;

import android.text.TextUtils;
import android.util.Log;

import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.interactor.IMainInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;
import com.music.dianyuetai.presenter.IMainPresenter;
import com.music.dianyuetai.presenter.SubscriptionManager;
import com.music.dianyuetai.utils.NetworkUtils;
import com.music.dianyuetai.view.MainView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月19日-11时27分
 */

public class MainPresenter implements IMainPresenter {

    private MainView view;
    private IMainInteractor interactor;
    private SubscriptionManager subscriptionManager;

    public MainPresenter(MainView view, IMainInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
        this.subscriptionManager = SubscriptionManager.getInstance();
    }

    @Override
    public void initializes() {
        User user = interactor.getUser();
        if (user != null) {
            Log.e("eeeee", user == null ? "user为空" : "user不为空");

            if (NetworkUtils.isOnline(view.getContext())) {
                Log.e("eeeee", "网络正常");
                subscriptionManager.addSubscription(
                        interactor.login(user, new LoginSubscriber())
                );
            } else {
                setUserInfo(user);
            }
        }
    }

    private void setUserInfo(User user) {
        view.setLoginViewVisible(true);
        view.setNoLoginViewVisible(false);

        String headerUrl = user.getIconUrl();
        String userName = user.getNickName();

        if (!TextUtils.isEmpty(headerUrl)) {
            view.setUserIcon(headerUrl);
        }

        if (TextUtils.isEmpty(userName)) {
            userName = user.getUsername();
        }

        view.setUserName(userName);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        subscriptionManager.removeAllSubscription();
    }

    private class LoginSubscriber extends BaseSubscriber<User> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }

        @Override
        public void onNext(User user) {

            if (user != null) {
                setUserInfo(user);
            }
        }
    }
}
