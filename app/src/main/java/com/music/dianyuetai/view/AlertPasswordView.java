package com.music.dianyuetai.view;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月21日-09时59分
 */

public interface AlertPasswordView extends BaseView{
    String getOldPassword();
    String getNewPassword();
    String getRepeatPassword();
    void toast(String info);
}
