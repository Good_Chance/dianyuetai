package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.Music;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-14时22分
 */

public interface RecommendView extends BaseView {
    void setRefreshState(int state);

    /**
     * 设置刷新数据
     *
     * @param list
     */
    void refresh(List<Music> list);
}
