package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.Favorite;

import java.util.List;

/**
 * Created by tang on 2016/10/19 0019.
 */

public interface FavoriteView extends BaseView {
    /**
     * 设置刷新数据
     *
     * @param list
     */
    void refresh(List<Favorite> list);

    void deleteAll();

    void delete(int position);
}
