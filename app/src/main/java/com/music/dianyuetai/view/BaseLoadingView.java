/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.view;

/**
 * 描述：view的基类接口定义,带有显示隐藏loading对话框的功能.
 * <br><br>
 * 作者：<b>欧阳文韬</b><br>
 * 日期：<b>2016年06月21日-14时00分</b>
 */
public interface BaseLoadingView extends BaseView{

    /**
     * 显示ProgressDialog
     *
     * @param flag 是否可退出
     * @param message 需要显示的信息
     */
    void showProgress(boolean flag, String message);

    /**
     * 隐藏ProgressDialog
     */
    void hideProgress();
}
