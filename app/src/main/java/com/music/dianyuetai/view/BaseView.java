/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.view;

import android.content.Context;

/**
 * 描述：view的基类接口定义.
 * <br><br>
 * 作者：<b>欧阳文韬</b><br>
 * 日期：<b>2016年06月21日-14时00分</b>
 */
public interface BaseView {
    Context getContext();
}
