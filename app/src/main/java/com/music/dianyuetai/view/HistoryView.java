package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.local.LocalHistory;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月22日-10时47分
 */

public interface HistoryView extends BaseView {
    /**
     * 设置刷新状态
     *
     * @param state
     */
    void setRefreshState(int state);

    /**
     * 设置刷新数据
     *
     * @param list
     */
    void refresh(List<LocalHistory> list);

    void deleteAll();

    void delete(int position);
}
