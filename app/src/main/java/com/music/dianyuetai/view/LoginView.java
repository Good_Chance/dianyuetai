package com.music.dianyuetai.view;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-14时38分
 */

public interface LoginView extends BaseView{
    String getAccount();
    String getPassword();
    void finish();
}
