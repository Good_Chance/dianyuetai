package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.Music;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月25日-15时33分
 */

public interface MusicsView {

    /**
     * 设置加载状态
     *
     * @param state
     */
    void setLoadState(int state);

    /**
     * 设置刷新状态
     *
     * @param state
     */
    void setRefreshState(int state);

    /**
     * 设置刷新数据
     *
     * @param list
     */
    void refresh(List<Music> list);

    /**
     * 设置加载更多数据
     *
     * @param list
     */
    void loadMore(List<Music> list);
}
