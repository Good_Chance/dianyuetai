package com.music.dianyuetai.view;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时14分
 */

public interface CategoryView extends BaseView {
    void setRefreshState(int state);

    /**
     * 设置刷新数据
     *
     * @param list
     */
    void refresh(List<List<String>> list);
}
