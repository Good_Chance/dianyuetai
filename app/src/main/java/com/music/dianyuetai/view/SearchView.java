package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.Music;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-13时17分
 */

public interface SearchView {
    String getSelection();

    void setRefreshState(int state);

    void setLoadState(int state);

    void refresh(List<Music> musicList);

    void loadMore(List<Music> musicList);
}
