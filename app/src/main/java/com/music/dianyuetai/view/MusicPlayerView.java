package com.music.dianyuetai.view;

import com.music.dianyuetai.bean.Comment;

import java.util.List;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月31日-11时15分
 */

public interface MusicPlayerView extends BaseView{
    void showPlayerPanel();

    void hidePlayerPanel();

    void setPreviewImage(String url);

    void setMusicName(String name);

    void setArtist(String artist);

    void setPainer(String painer);

    String getMusicId();

    void loadComment(List<Comment> comments);

    void setAppBarDisable();

    /**
     * 设置加载状态
     *
     * @param state
     */
    void setLoadState(int state);

    /**
     * 设置刷新状态
     *
     * @param state
     */
    void setRefreshState(int state);

    /**
     * 设置播放进度
     *
     * @param max      总长度
     * @param progress 播放进度
     */
    void setProgress(long max, long progress);

    /**
     * 设置缓冲进度
     *
     * @param percent
     */
    void setBufferProgress(long percent);

    /**
     * 设置播放按钮状态
     *
     * @param isPlaying true 播放中 false 暂停
     */
    void setPlayBtnState(boolean isPlaying);

    /**
     * 设置收藏按钮状态
     *
     * @param isFavorite true 已收藏 false 未收藏
     */
    void setFavoriteState(boolean isFavorite);
}
