package com.music.dianyuetai.view;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月19日-11时18分
 */

public interface MainView extends BaseView {
    void setUserIcon(String iconUrl);
    void setUserName(String userName);
    void setNoLoginViewVisible(boolean visible);
    void setLoginViewVisible(boolean visible);
}
