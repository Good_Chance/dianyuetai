package com.music.dianyuetai;

import android.app.Application;
import android.content.Context;

import com.kingsoft.media.httpcache.KSYProxyService;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.constant.BmobKey;
import com.music.dianyuetai.db.DaoMaster;
import com.music.dianyuetai.db.DaoSession;
import com.music.dianyuetai.utils.CrashHandler;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;


/**
 * Created by Tang on 2016/8/17 0017.
 */
public class App extends Application {
    private static final String DB_NAME = "dianyuetai";

    private static App mInstance;

    private DaoSession mDaoSession;

    private KSYProxyService proxy;

    private User user;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //初始化Bmob
        Bmob.initialize(this, BmobKey.APPLICATION_ID);
        //初始化数据库
        setupDatabase();

       // refWatcher = LeakCanary.install(this);

        user = BmobUser.getCurrentUser(User.class);

       //CrashHandler.getInstance().init(this);
    }

    /**
     * 获取全局Context
     *
     * @return
     */
    public static App getAppContext() {
        return mInstance;
    }

    /**
     * 初始化本地数据库
     */
    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, DB_NAME, null);
        DaoMaster daoMaster = new DaoMaster(helper.getWritableDatabase());
        mDaoSession = daoMaster.newSession();
    }

    public static KSYProxyService getProxy(Context context) {
        App app = (App) context.getApplicationContext();
        if (app.proxy == null) {
            app.proxy = app.newProxy();
        }

        return app.proxy;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return this.user;
    }

    private KSYProxyService newProxy() {
        return new KSYProxyService(this);
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }


  /*  //TODO 检测内存泄漏，测试用
    public static RefWatcher getRefWatcher(Context context) {
        App application = (App) context.getApplicationContext();
        return application.refWatcher;
    }

    private RefWatcher refWatcher;*/
}
