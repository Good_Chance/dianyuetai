package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.FavoriteApi;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.interactor.IFavoriteInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tang on 2016/10/19 0019.
 */

public class FavoriteInteractor implements IFavoriteInteractor {
    @Override
    public Subscription getFavoriteList(String userId, BaseSubscriber<List<Favorite>> subscriber) {
      return  FavoriteApi.getFavoriteList(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public Subscription deleteFavorite(Favorite favorite, BaseSubscriber<Void> subscriber) {
        return FavoriteApi.deleteFavorite(favorite.getObjectId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
