package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.App;
import com.music.dianyuetai.api.UserApi;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.interactor.IMainInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月19日-11时24分
 */

public class MainInteractor implements IMainInteractor {
    @Override
    public User getUser() {
        return App.getAppContext().getUser();
    }

    @Override
    public void setUser(User user) {

    }

    @Override
    public Subscription login(User user, BaseSubscriber<User> subscriber) {
        return UserApi.login(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
