package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.presenter.BaseSubscriber;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月19日-11时22分
 */

public interface IMainInteractor {
    /**
     * 获取当前User对象
     *
     * @return
     */
    User getUser();

    /**
     * 保存User对象
     *
     * @param user
     */
    void setUser(User user);

    /**
     * 登录
     *
     * @param user 用户实体对象
     * @param subscriber
     * @return
     */
    Subscription login(User user, BaseSubscriber<User> subscriber);
}
