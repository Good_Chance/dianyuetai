package com.music.dianyuetai.interactor;

import cn.bmob.v3.listener.UpdateListener;
import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月21日-10时02分
 */

public interface IAlertPasswordInteractor {
    /**
     * 修改登录密码
     *
     * @param oldPassword 原密码
     * @param newPassword 新密码
     * @param listener
     * @return
     */
    Subscription alertPassword(String oldPassword, String newPassword, UpdateListener listener);
}
