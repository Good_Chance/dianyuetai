package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;

/**
 * Created by tang on 2016/10/19 0019.
 */

public interface IFavoriteInteractor {
    /**
     * 获取用户收藏列表
     *
     * @param userId     用户ObjectId
     * @param subscriber
     * @return
     */
    Subscription getFavoriteList(String userId, BaseSubscriber<List<Favorite>> subscriber);

    /**
     * 删除收藏
     *
     * @param favorite   收藏实体类
     * @param subscriber
     * @return
     */
    Subscription deleteFavorite(Favorite favorite, BaseSubscriber<Void> subscriber);
}
