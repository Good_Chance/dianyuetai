package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月25日-14时41分
 */

public interface IMusicInteractor {
    /**
     * 获取歌曲列表
     *
     * @param page       列表页码
     * @param order      排序条件
     * @param subscriber
     * @return
     */
    Subscription getMusicList(int page, String order, BaseSubscriber<List<Music>> subscriber);
}
