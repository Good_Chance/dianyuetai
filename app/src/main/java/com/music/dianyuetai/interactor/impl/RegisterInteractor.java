package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.UserApi;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.interactor.IRegisterInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-16时26分
 */

public class RegisterInteractor implements IRegisterInteractor {
    @Override
    public Subscription register(String account, String nickName, String password, BaseSubscriber<User> subscriber) {
        return UserApi.register(account, nickName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
