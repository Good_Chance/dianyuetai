package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.MusicApi;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.interactor.IMusicInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月25日-15时32分
 */

public class MusicInteractor implements IMusicInteractor {
    @Override
    public Subscription getMusicList(int page, String order, BaseSubscriber<List<Music>> subscriber) {
        return MusicApi.getListByOrder(page, order)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
