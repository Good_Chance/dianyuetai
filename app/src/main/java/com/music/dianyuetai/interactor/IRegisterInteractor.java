package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.presenter.BaseSubscriber;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-16时25分
 */

public interface IRegisterInteractor {
    /**
     * 注册账号
     *
     * @param account    账号
     * @param nickName   昵称
     * @param password   密码
     * @param subscriber 注册结果回调
     * @return
     */
    Subscription register(String account, String nickName, String password, BaseSubscriber<User> subscriber);
}
