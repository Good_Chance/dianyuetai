package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-13时13分
 */

public interface ISearchInteractor {
    /**
     * 获取搜索结果
     *
     * @param page       查询数据页码
     * @param musicName  要搜索的音乐名
     * @param subscriber 查询结果回调
     * @return
     */
    Subscription getSearchList(int page, String musicName, BaseSubscriber<List<Music>> subscriber);
}
