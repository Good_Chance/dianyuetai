package com.music.dianyuetai.interactor.impl;

import android.util.Log;
import android.view.View;

import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.db.DaoManager;
import com.music.dianyuetai.interactor.IHistoryInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月20日-14时21分
 */

public class HistoryInteractor implements IHistoryInteractor {
    @Override
    public Subscription getHistoryList(BaseSubscriber<List<LocalHistory>> subscriber) {
        return Observable
                .create(new Observable.OnSubscribe<List<LocalHistory>>() {
                    @Override
                    public void call(Subscriber<? super List<LocalHistory>> subscriber) {
                        List<LocalHistory> list = DaoManager.getInstance().getHistoryList();
                        Log.e("历史记录大小", "记录大小：：：" + (list == null ? -1 : list.size()));
                        subscriber.onNext(list);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public Subscription deleteAllHistory() {
        return Observable
                .create(new Observable.OnSubscribe<Void>() {
                    @Override
                    public void call(Subscriber<? super Void> subscriber) {
                        DaoManager.getInstance().deleteAllHistory();
                        subscriber.onNext(null);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }
}
