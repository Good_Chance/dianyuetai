package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.MusicApi;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.interactor.ISearchInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-13时14分
 */

public class SearchInteractor implements ISearchInteractor {
    @Override
    public Subscription getSearchList(int page, String musicName, BaseSubscriber<List<Music>> subscriber) {
        return MusicApi.getListBySelection(page, musicName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
