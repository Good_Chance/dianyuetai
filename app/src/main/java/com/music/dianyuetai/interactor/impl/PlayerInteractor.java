package com.music.dianyuetai.interactor.impl;

import android.util.Log;

import com.music.dianyuetai.api.CommentApi;
import com.music.dianyuetai.api.FavoriteApi;
import com.music.dianyuetai.api.MusicApi;
import com.music.dianyuetai.bean.Comment;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.db.DaoManager;
import com.music.dianyuetai.interactor.IPlayerInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月31日-10时52分
 */

public class PlayerInteractor implements IPlayerInteractor {

    @Override
    public Subscription getMusicDetail(String objectId, BaseSubscriber<Music> subscriber) {
        return MusicApi.getMusic(objectId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public Subscription getComments(String objectId, int page, BaseSubscriber<List<Comment>> subscriber) {
        return CommentApi.getCommentList(page, objectId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public Subscription sendComment(Comment comment, BaseSubscriber<String> subscriber) {
        return CommentApi.addComment(comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public Subscription addFavorite(final Favorite favorite, final BaseSubscriber<Boolean> subscriber) {
        return FavoriteApi.isFavorite(favorite.getUserId(), favorite.getMusic().getObjectId())
                .subscribeOn(Schedulers.io())
                .map(new Func1<Favorite, Boolean>() {
                    @Override
                    public Boolean call(Favorite favorite1) {
                        if (favorite1 != null) {
                            //取消收藏
                            FavoriteApi.deleteFavorite(favorite1.getObjectId());
                            return false;
                        } else {
                            //添加收藏
                            FavoriteApi.addFavorite(favorite);
                            return true;
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

    @Override
    public void addHistory(LocalHistory history) {
        //添加进本地数据库
        Observable.just(history)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Action1<LocalHistory>() {
                    @Override
                    public void call(LocalHistory history) {
                        DaoManager.getInstance().updateOrInsertHistory(history);
                        Log.e("playerInteractor", "添加或更新记录");
                    }
                });
    }

    @Override
    public long addLocalMusic(LocalMusic localMusic) {
        return DaoManager.getInstance().updateOrInsertMusic(localMusic);
    }
}
