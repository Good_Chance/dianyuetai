package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.bean.Comment;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年08月31日-10时52分
 */

public interface IPlayerInteractor {
    /**
     * 获取歌曲详细数据
     *
     * @param objectId   歌曲ObjectId
     * @param subscriber
     * @return
     */
    Subscription getMusicDetail(String objectId, BaseSubscriber<Music> subscriber);

    /**
     * 获取评论列表
     *
     * @param objectId   歌曲ObjectId
     * @param page       评论页码
     * @param subscriber
     * @return
     */
    Subscription getComments(String objectId, int page, BaseSubscriber<List<Comment>> subscriber);

    /**
     * 发送评论
     *
     * @param comment    评论实体对象
     * @param subscriber
     * @return
     */
    Subscription sendComment(Comment comment, BaseSubscriber<String> subscriber);

    /**
     * 添加/删除一条收藏记录
     *
     * @param favorite   收藏实体对象
     * @param subscriber
     * @return
     */
    Subscription addFavorite(Favorite favorite, BaseSubscriber<Boolean> subscriber);

    /**
     * 添加/更新历史记录
     *
     * @param history
     */
    void addHistory(LocalHistory history);

    /**
     * 添加歌曲到本地数据库
     *
     * @param localMusic 本地歌曲实体对象
     * @return
     */
    long addLocalMusic(LocalMusic localMusic);
}
