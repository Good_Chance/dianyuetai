package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月20日-14时20分
 */

public interface IHistoryInteractor {
    /**
     * 获取历史记录列表
     *
     * @param subscriber
     * @return
     */
    Subscription getHistoryList(BaseSubscriber<List<LocalHistory>> subscriber);

    /**
     * 删除所有历史记录
     *
     * @return
     */
    Subscription deleteAllHistory();
}
