package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.interactor.ICategoryInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时17分
 */

public class CategoryInteractor implements ICategoryInteractor {
    @Override
    public Subscription getCategoryList(BaseSubscriber<List<List<String>>> subscriber) {

        return Observable
                .create(new Observable.OnSubscribe<List<List<String>>>() {
                    @Override
                    public void call(Subscriber<? super List<List<String>>> subscriber) {
                        List<String> styleList = new ArrayList<>();
                        styleList.add("HOUSE");
                        styleList.add("TECHNO");
                        styleList.add("Trance");
                        styleList.add("Disco");
                        styleList.add("Synth pop");
                        styleList.add("Hip-hop");
                        styleList.add("Acid jazz");
                        styleList.add("New Age");
                        styleList.add("Amebient");


                        List<List<String>> categoryList = new ArrayList<>();
                        categoryList.add(styleList);
                        subscriber.onNext(categoryList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
