package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.MusicApi;
import com.music.dianyuetai.api.RecommendApi;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.bean.Recommend;
import com.music.dianyuetai.interactor.IRecommendInteractor;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-14时24分
 */

public class RecommendInteractor implements IRecommendInteractor {
    @Override
    public Subscription getMusicList(BaseSubscriber<List<Music>> subscriber) {
        return MusicApi.getMusicList(1, 4, null, null, "playCount", "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    /*    return RecommendApi.getRecommendList()
                .subscribeOn(Schedulers.io())
                .map(new Func1<List<Recommend>, List<Music>>() {
                    @Override
                    public List<Music> call(List<Recommend> recommends) {
                        List<Music> musicList = null;
                        if (recommends != null && recommends.size() > 0) {
                            musicList = new ArrayList<>();
                            for (Recommend recommend : recommends) {
                                musicList.add(recommend.getMusic());
                            }
                        }
                        return musicList;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);*/
    }
}
