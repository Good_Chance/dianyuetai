package com.music.dianyuetai.interactor;

import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时15分
 */

public interface ICategoryInteractor {
    /**
     * 获取分类数据
     *
     * @param obsever
     * @return
     */
    Subscription getCategoryList(BaseSubscriber<List<List<String>>> obsever);
}
