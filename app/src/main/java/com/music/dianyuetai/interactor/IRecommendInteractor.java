package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.presenter.BaseSubscriber;

import java.util.List;

import rx.Subscription;


/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月12日-14时23分
 */

public interface IRecommendInteractor {
    /**
     * 获取推荐页音乐列表
     *
     * @param obsever 返回结果回调
     * @return
     */
    Subscription getMusicList(BaseSubscriber<List<Music>> obsever);
}
