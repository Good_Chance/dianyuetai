package com.music.dianyuetai.interactor;

import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.presenter.BaseSubscriber;

import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月09日-14时42分
 */

public interface ILoginInteractor {
    /**
     * 登录
     *
     * @param account    账号
     * @param password   密码
     * @param subscriber
     * @return
     */
    Subscription login(String account, String password, BaseSubscriber<User> subscriber);
}
