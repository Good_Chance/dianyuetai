package com.music.dianyuetai.interactor.impl;

import com.music.dianyuetai.api.UserApi;
import com.music.dianyuetai.interactor.IAlertPasswordInteractor;

import cn.bmob.v3.listener.UpdateListener;
import rx.Subscription;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月21日-10时02分
 */

public class AlertPasswordInteractor implements IAlertPasswordInteractor {
    @Override
    public Subscription alertPassword(String oldPassword, String newPassword, UpdateListener listener) {
        return UserApi.updatePassword(oldPassword, newPassword, listener);
    }
}
