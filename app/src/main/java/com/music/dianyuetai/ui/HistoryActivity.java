package com.music.dianyuetai.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.marst.mxrecyclerview.SwipRefreshRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.HistoryAdapter;
import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.interactor.impl.HistoryInteractor;
import com.music.dianyuetai.presenter.IHistoryPresenter;
import com.music.dianyuetai.presenter.impl.HistoryPresenter;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.music.dianyuetai.utils.ToastUtils;
import com.music.dianyuetai.utils.UIUtils;
import com.music.dianyuetai.view.HistoryView;

import java.util.List;

import butterknife.BindView;

public class HistoryActivity extends BaseActivity implements HistoryView, View.OnClickListener {

    @BindView(R.id.recycler_view)
    SwipRefreshRecyclerView recyclerView;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.image_delete_history)
    ImageView imageDeleteHistory;

    private HistoryAdapter adapter;

    private IHistoryPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_history;
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);
        imageDeleteHistory.setOnClickListener(this);

        adapter = new HistoryAdapter(this, null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setLoadingMoreEnabled(false);
        recyclerView.setColorSchemeResources(R.color.tomato, R.color.default_background_color);
        recyclerView.setProgressViewOffset(true, UIUtils.dp2Px(50), UIUtils.dp2Px(250));
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<LocalHistory>() {
            @Override
            public void onItemClick(View view, int position, LocalHistory data) {
                Bundle bundle = new Bundle();
                bundle.putString(Extra.MUSIC_ID, data.getMusicObjectId());
                forward(MusicPlayerActivity.class, bundle);
            }
        });

        recyclerView.setOnLoadingListener(new MxRecyclerView.OnLoadingListener() {
            @Override
            public void onRefresh() {
                presenter.initializes();
            }

            @Override
            public void onLoadMore() {

            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new HistoryPresenter(this, new HistoryInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void refresh(List<LocalHistory> list) {
        adapter.replace(list);
    }

    @Override
    public void deleteAll() {
        adapter.removeAll();
    }

    @Override
    public void delete(int position) {
        adapter.remove(position);
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                break;
            case R.id.image_delete_history:
                if (adapter.getItemCount() == 0) {
                    ToastUtils.show("还没有任何记录哦~");
                } else {
                    showDeleteDialog();
                }
            default:
                break;
        }
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //设置Title的图标
        builder.setIcon(R.drawable.ic_error_outline);
        builder.setTitle("清空记录");
        builder.setMessage("真的要清空吗？(๑˙ー˙๑)");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.deleteAll();
            }
        });
        builder.setNegativeButton("取消", null);

        builder.show();
    }
}
