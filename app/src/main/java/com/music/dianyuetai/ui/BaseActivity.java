package com.music.dianyuetai.ui;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.music.dianyuetai.utils.ToastUtils;
import com.zhy.autolayout.AutoLayoutActivity;

import java.util.List;

import butterknife.ButterKnife;

/**
 * 描述：Activity基类
 * 作者：唐志远
 * 日期：2016年06月22日-08时49分
 */
public abstract class BaseActivity extends AutoLayoutActivity {
    protected View mLayoutView;

    /**
     * debug用tag标签。
     */
    protected String tag = this.getClass().getName();
    private ProgressDialog mProgressDialog;

    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLayoutView = getLayoutInflater().inflate(getLayoutRes(), null);
        if (mLayoutView != null) {
            setContentView(mLayoutView);
            ButterKnife.bind(this);
        }
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        onBefore(savedInstanceState);
        initUiAndListener();
        initPresenter();
        initData();
    }

    /**
     * 在初始化控件之前的某些操作，例如：获取intent传递的参数
     */
    protected void onBefore(Bundle savedInstanceState) {

    }

    /**
     * 用于获取activity界面布局资源Id。
     *
     * @return 布局资源Id
     */
    public abstract int getLayoutRes();

    /**
     * 初始化控件、设置监听
     */
    public abstract void initUiAndListener();

    /**
     * 初始化控制器
     */
    public abstract void initPresenter();

    /**
     * 初始化数据操作，例如：网络数据
     */
    protected abstract void initData();

    /**
     * 切换到后台
     */
    protected void onBackground() {
        Log.d(tag, "onBackground:执行。");
    }

    /**
     * 切换到前台
     */
    protected void onForeground() {
        Log.d(tag, "onBackground:执行。");
    }

    /**
     * 显示ProgressDialog
     *
     * @param isCancelable 是否能够退出
     * @param message      需要显示的信息
     */
    public void showProgress(boolean isCancelable, String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(isCancelable);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    /**
     * 隐藏ProgressDialog
     */
    public void hideProgress() {
        if (mProgressDialog == null)
            return;
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * 显示Toast
     *
     * @param info
     */
    public void toast(String info) {
        ToastUtils.show(info);
    }

    public void forward(Class<?> cls) {
        forward(cls, null);
    }

    public void forward(Class<?> cls, Bundle bundle) {
        forward(cls, bundle, -1);
    }

    public void forward(Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent(this, cls);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        if (requestCode != -1) {
            startActivityForResult(intent, requestCode);
        } else {
            startActivity(intent);
        }
    }

    /**
     * 显示软键盘
     */
    public void showSoftInput() {
        try {
            inputMethodManager.showSoftInput(mLayoutView, InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {

        }
    }

    /**
     * 隐藏软键盘
     */
    public void hideSoftInput() {
        try {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(tag, "onResume:执行。");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(tag, "onPause:执行。");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(tag, "onStop:执行。");
        if (!isOnForeground()) {
            //app进入后台。
            onBackground();
        }
    }

    @Override
    public void finish() {
        super.finish();
        Log.d(tag, "finish:执行。");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(tag, "onDestroy:执行。");
    }

    /**
     * 程序是否在前台运行
     *
     * @return true(是在前台运行)、false(是在后台运行)
     */
    private boolean isOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null)
            return false;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }
}
