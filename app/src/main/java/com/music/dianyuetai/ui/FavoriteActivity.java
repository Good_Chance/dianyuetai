package com.music.dianyuetai.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.FavoriteAdapter;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.interactor.impl.FavoriteInteractor;
import com.music.dianyuetai.presenter.IFavoritePresenter;
import com.music.dianyuetai.presenter.impl.FavoritePresenter;
import com.music.dianyuetai.view.FavoriteView;

import java.util.List;

import butterknife.BindView;

public class FavoriteActivity extends BaseActivity implements View.OnClickListener, FavoriteView {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private FavoriteAdapter adapter;

    private IFavoritePresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_favorite;
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);

        adapter = new FavoriteAdapter(this, null);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Favorite>() {
            @Override
            public void onItemClick(View view, int position, Favorite data) {
                Bundle bundle = new Bundle();
                bundle.putString(Extra.MUSIC_ID, data.getMusic().getObjectId());
                forward(MusicPlayerActivity.class, bundle);
            }
        });

        adapter.setOnItemLongClickListener(new BaseRecyclerAdapter.OnItemLongClickListener<Favorite>() {
            @Override
            public void onItemLongClick(View view, int position, Favorite data) {

            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new FavoritePresenter(this, new FavoriteInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void refresh(List<Favorite> list) {
        adapter.replace(list);
    }

    @Override
    public void deleteAll() {
        adapter.removeAll();
    }

    @Override
    public void delete(int position) {
        adapter.remove(position);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
