package com.music.dianyuetai.ui;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.interactor.impl.LoginInteractor;
import com.music.dianyuetai.presenter.ILoginPresenter;
import com.music.dianyuetai.presenter.impl.LoginPresenter;
import com.music.dianyuetai.view.LoginView;

import butterknife.BindView;

public class LoginActivity extends BaseActivity implements LoginView, View.OnClickListener {
    @BindView(R.id.edit_account)
    EditText editAccount;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.text_login)
    TextView textLogin;
    @BindView(R.id.text_register)
    TextView textRegister;
    @BindView(R.id.text_retrieve)
    TextView textRetrieve;
    @BindView(R.id.image_back)
    ImageView imageBack;

    private ILoginPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);
        textLogin.setOnClickListener(this);
        textRegister.setOnClickListener(this);
        textRetrieve.setOnClickListener(this);
    }

    @Override
    public void initPresenter() {
        presenter = new LoginPresenter(this, new LoginInteractor());
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public String getAccount() {
        return editAccount.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return editPassword.getText().toString().trim();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.text_login:
                //登录
                presenter.login();
                break;
            case R.id.text_register:
                //进入注册页
                forward(RegisterActivity.class);
                break;
            case R.id.text_retrieve:
                //进入找回密码页
                forward(ForgotActivity.class);
                break;
            default:
                break;
        }
    }
}
