package com.music.dianyuetai.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.CommentAdapter;
import com.music.dianyuetai.bean.Comment;
import com.music.dianyuetai.interactor.impl.PlayerInteractor;
import com.music.dianyuetai.presenter.IPlayerPresenter;
import com.music.dianyuetai.presenter.impl.PlayerPresenter;
import com.music.dianyuetai.utils.DateUtils;
import com.music.dianyuetai.utils.DisableableAppBarLayoutBehavior;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.music.dianyuetai.view.MusicPlayerView;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.emoji.Emoji;
import com.vanniktech.emoji.listeners.OnEmojiBackspaceClickListener;
import com.vanniktech.emoji.listeners.OnEmojiClickedListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardOpenListener;

import java.util.List;

import butterknife.BindView;

public class MusicPlayerActivity extends BaseActivity implements View.OnClickListener, MusicPlayerView {

    @BindView(R.id.image_preview)
    ImageView imagePreview;
    @BindView(R.id.image_play)
    ImageView imagePlay;
    @BindView(R.id.layout_timer)
    LinearLayout layoutTimer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar_layout)
    AppBarLayout appbarLayout;
    @BindView(R.id.seekbar)
    AppCompatSeekBar seekbar;
    @BindView(R.id.text_artist)
    TextView textArtist;
    @BindView(R.id.tv_pic_label)
    TextView tvPicLabel;
    @BindView(R.id.text_painer)
    TextView textPainer;
    @BindView(R.id.tv_count_play)
    TextView tvCountPlay;
    @BindView(R.id.text_count_comment)
    TextView tvCountComment;
    @BindView(R.id.tv_count_favorite)
    TextView tvCountFavorite;
    @BindView(R.id.text_description)
    TextView tvEmDesc;
    @BindView(R.id.text_music_title)
    TextView textMusicTitle;
    @BindView(R.id.recycler_view)
    MxRecyclerView recyclerView;
    @BindView(R.id.nested_scrollview)
    NestedScrollView nestedScrollview;
    @BindView(R.id.text_current_time)
    TextView textCurrentTime;
    @BindView(R.id.text_duration)
    TextView textDuration;
    @BindView(R.id.image_favorite)
    ImageView imageFavorite;
    @BindView(R.id.image_comment)
    ImageView imageComment;
    @BindView(R.id.image_download)
    ImageView imageDownload;
    @BindView(R.id.layout_bottom)
    CardView layoutBottom;
    @BindView(R.id.layout_favorite)
    LinearLayout layoutFavorite;
    @BindView(R.id.layout_comment)
    LinearLayout layoutComment;
    @BindView(R.id.layout_download)
    LinearLayout layoutDownload;

    @BindView(R.id.layout_01)
    View layout01;
    @BindView(R.id.layout_02)
    View layout02;
    @BindView(R.id.btn_emoji)
    ImageButton btnEmoji;
    @BindView(R.id.edit_comment)
    EmojiEditText editMessage;
    @BindView(R.id.text_send_comment)
    TextView textSendMessage;

    private EmojiPopup emojiPopup;


    //
    private String musicId;

    private IPlayerPresenter presenter;

    private CommentAdapter adapter;

    private Animation bottomShowAnimation;
    private Animation bottomHideAnimation;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_music_player;
    }

    @Override
    protected void onBefore(Bundle savedInstanceState) {
        super.onBefore(savedInstanceState);
        musicId = getIntent().getExtras().getString(Extra.MUSIC_ID);
    }

    @Override
    public void initUiAndListener() {
        appbarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    //展开状态
                    seekbar.setVisibility(View.VISIBLE);
                    showPlayerPanel();
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    //折叠状态
                    seekbar.setVisibility(View.GONE);
                } else {
                    //滑动状态
                    hidePlayerPanel();
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imagePlay.setOnClickListener(this);

        //初始化评论列表控件
        adapter = new CommentAdapter(this, null);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Comment>() {
            @Override
            public void onItemClick(View view, int position, Comment data) {

            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setPullRefreshEnabled(false);

        nestedScrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    //下划
                    if (layoutBottom.getVisibility() == View.VISIBLE) {
                        layoutBottom.startAnimation(bottomHideAnimation);
                        layoutBottom.setVisibility(View.GONE);
                    }
                }
                if (scrollY < oldScrollY) {
                    //上划
                    if (layoutBottom.getVisibility() == View.GONE) {
                        layoutBottom.startAnimation(bottomShowAnimation);
                        layoutBottom.setVisibility(View.VISIBLE);
                    }
                }

                if (scrollY == 0) {
                    //到顶了
                }

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    //到底了
                    presenter.loadComment();
                }
            }
        });

        //初始化SeekBar
        seekbar.setPadding(0, 0, 0, 0);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                presenter.seekTo(seekBar.getProgress());
                textCurrentTime.setText(DateUtils.formatPlay(seekBar.getProgress()));
            }
        });

        layoutFavorite.setOnClickListener(this);
        layoutComment.setOnClickListener(this);
        layoutDownload.setOnClickListener(this);

        btnEmoji.setOnClickListener(this);
        textSendMessage.setOnClickListener(this);

        //评论发送EditText添加内容改变监听
        editMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence)) {
                    textSendMessage.setText("发送");
                } else {
                    textSendMessage.setText("取消");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setUpEmojiPopup();
    }

    @Override
    public void initPresenter() {
        presenter = new PlayerPresenter(this, new PlayerInteractor());
    }

    @Override
    protected void initData() {
        bottomShowAnimation = AnimationUtils.loadAnimation(this, R.anim.push_bottom_in);
        bottomShowAnimation.setInterpolator(new DecelerateInterpolator());
        bottomHideAnimation = AnimationUtils.loadAnimation(this, R.anim.push_bottom_out);
        bottomHideAnimation.setInterpolator(new AccelerateInterpolator());

        presenter.initializes();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_play:
                presenter.play();
                break;
            case R.id.layout_favorite:
                presenter.favorite();
                break;
            case R.id.layout_comment:
                layout01.setVisibility(View.GONE);
                layout02.setVisibility(View.VISIBLE);
                //发送评论
                break;
            case R.id.layout_download:
                //下载歌曲和背景图片
                presenter.download();
                break;
            case R.id.btn_emoji:
                emojiPopup.toggle();
                break;
            case R.id.text_send_comment:
                String text = textSendMessage.getText().toString();

                if (TextUtils.equals("取消", text)) {
                    layout01.setVisibility(View.VISIBLE);
                    layout02.setVisibility(View.GONE);
                    hideSoftInput();
                } else if (TextUtils.equals("发送", text)) {
                    presenter.sendComment();
                }
            default:
                break;
        }
    }

    private void setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView(mLayoutView).setOnEmojiBackspaceClickListener(new OnEmojiBackspaceClickListener() {
            @Override
            public void onEmojiBackspaceClicked(final View v) {
                Log.d("MainActivity", "Clicked on Backspace");
            }
        }).setOnEmojiClickedListener(new OnEmojiClickedListener() {
            @Override
            public void onEmojiClicked(final Emoji emoji) {
                Log.d("MainActivity", "Clicked on emoji");
            }
        }).setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
            @Override
            public void onEmojiPopupShown() {
                btnEmoji.setImageResource(R.drawable.ic_comment);
            }
        }).setOnSoftKeyboardOpenListener(new OnSoftKeyboardOpenListener() {
            @Override
            public void onKeyboardOpen(final int keyBoardHeight) {
                Log.d("MainActivity", "Opened soft keyboard");
            }
        }).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
            @Override
            public void onEmojiPopupDismiss() {
                btnEmoji.setImageResource(R.drawable.emoji_people);
            }
        }).setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
            @Override
            public void onKeyboardClose() {
                emojiPopup.dismiss();
            }
        }).build(editMessage);
    }

    @Override
    public void showPlayerPanel() {
        layoutTimer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePlayerPanel() {
        layoutTimer.setVisibility(View.GONE);
    }

    @Override
    public void setPreviewImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            ImageUtil.loadImg(imagePreview, url);
        }
    }

    @Override
    public void setMusicName(String name) {
        if (!TextUtils.isEmpty(name)) {
            textMusicTitle.setText(name);
        }
    }

    @Override
    public void setArtist(String artist) {
        textArtist.setText(artist);
    }

    @Override
    public void setPainer(String painer) {
        if (painer != null) {
            textPainer.setText(painer);
        } else {
            textPainer.setVisibility(View.GONE);
            tvPicLabel.setVisibility(View.GONE);
        }
    }

    @Override
    public String getMusicId() {
        return musicId;
    }

    @Override
    public void loadComment(List<Comment> comments) {
        adapter.append(comments);
    }

    @Override
    public void setAppBarDisable() {
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appbarLayout.getLayoutParams();
        layoutParams.setBehavior(new DisableableAppBarLayoutBehavior());
    }

    @Override
    public void setLoadState(int state) {
        RecyclerViewUtils.setLoadState(recyclerView, state);
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void setProgress(long max, long progress) {
        seekbar.setMax((int) max);
        seekbar.setProgress((int) progress);
        textDuration.setText(DateUtils.formatPlay(max));
        textCurrentTime.setText(DateUtils.formatPlay(progress));
    }

    @Override
    public void setBufferProgress(long percent) {
        seekbar.setSecondaryProgress((int) percent);
    }

    @Override
    public void setPlayBtnState(boolean isPlaying) {
        imagePlay.setSelected(isPlaying);
    }

    @Override
    public void setFavoriteState(boolean isFavorite) {
        imageFavorite.setSelected(isFavorite);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
