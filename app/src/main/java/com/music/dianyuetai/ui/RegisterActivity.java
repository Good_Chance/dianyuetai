package com.music.dianyuetai.ui;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.interactor.impl.RegisterInteractor;
import com.music.dianyuetai.presenter.IRegisterPresenter;
import com.music.dianyuetai.presenter.impl.RegisterPresenter;
import com.music.dianyuetai.view.RegisterView;

import butterknife.BindView;

public class RegisterActivity extends BaseActivity implements View.OnClickListener, RegisterView {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.btn_register)
    TextView btnRegister;
    @BindView(R.id.edit_account)
    EditText editAccount;
    @BindView(R.id.edit_nickname)
    EditText editNickname;
    @BindView(R.id.edit_password)
    EditText editPassword;

    private IRegisterPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_register;
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void initPresenter() {
        presenter = new RegisterPresenter(this, new RegisterInteractor());
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.btn_register:
                //注册
                presenter.register();
                break;
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public String getAccount() {
        return editAccount.getText().toString().trim();
    }

    @Override
    public String getNickName() {
        return editNickname.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return editPassword.getText().toString().trim();
    }
}
