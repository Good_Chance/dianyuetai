package com.music.dianyuetai.ui;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.marst.mxrecyclerview.SwipRefreshRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.CategoryAdapter;
import com.music.dianyuetai.interactor.impl.CategoryInteractor;
import com.music.dianyuetai.presenter.ICategoryPresenter;
import com.music.dianyuetai.presenter.impl.CategoryPresenter;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.music.dianyuetai.utils.UIUtils;
import com.music.dianyuetai.view.CategoryView;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends BaseFragment implements CategoryView {

    @BindView(R.id.recycler_view)
    SwipRefreshRecyclerView recyclerView;

    private CategoryAdapter adapter;

    private ICategoryPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_category;
    }

    @Override
    public void initUiAndListener() {
        adapter = new CategoryAdapter(getContext(), null);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.setLoadingMoreEnabled(false);
        recyclerView.setColorSchemeResources(R.color.tomato, R.color.default_background_color);
        recyclerView.setProgressViewOffset(true, UIUtils.dp2Px(50), UIUtils.dp2Px(250));

        recyclerView.setOnLoadingListener(new MxRecyclerView.OnLoadingListener() {
            @Override
            public void onRefresh() {
                presenter.refresh();
            }

            @Override
            public void onLoadMore() {

            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new CategoryPresenter(this, new CategoryInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void refresh(List<List<String>> list) {
        adapter.replace(list);
    }
}
