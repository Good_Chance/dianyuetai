package com.music.dianyuetai.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.DownloadAdapter;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.download.DownloadManager;
import com.music.dianyuetai.download.DownloadService;
import com.music.dianyuetai.event.UpdateDownloadEvent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.music.dianyuetai.download.DownloadManager.COMPLETED;

public class DownloadManagerActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private DownloadAdapter adapter;

    private DownloadManager downloadManager;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_download_manager;
    }

    @Override
    protected void onBefore(Bundle savedInstanceState) {
        RxBus.get().register(this);
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);
    }

    @Override
    public void initPresenter() {
        adapter = new DownloadAdapter(this, null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Download>() {
            @Override
            public void onItemClick(View view, int position, Download data) {
                if (data.getDownloadState() == COMPLETED) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Extra.MUSIC_ID, data.getMusicObjectId());
                    forward(MusicPlayerActivity.class, bundle);
                }
            }
        });
    }

    @Override
    protected void initData() {
        downloadManager = DownloadService.getDownloadManager(this);

        updateList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void updateDownloadList(UpdateDownloadEvent event) {
        updateList();
    }

    public void updateList() {
        List<Download> downloadList = new ArrayList<>();
        downloadList.addAll(downloadManager.getTaskList());
        downloadList.addAll(downloadManager.getCompleteList());

        Log.e("下载管理", "下载管理页任务数量：：" + downloadList.size());

        adapter.replace(downloadList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
                break;
            default:
                break;
        }
    }
}
