package com.music.dianyuetai.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.music.dianyuetai.utils.ToastUtils;

import butterknife.ButterKnife;

/**
 * 描述：Fragment基类
 * 作者：唐志远
 * 日期：2016年06月22日-08时49分
 */
public abstract class BaseFragment extends Fragment {
    private BaseActivity mActivity;

    protected View mLayoutView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onBefore();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLayoutView = inflater.inflate(getLayoutRes(), container, false);
        ButterKnife.bind(this, mLayoutView);
        initUiAndListener();
        initPresenter();
        initData();
        return mLayoutView;
    }

    /**
     * 获取fragment状态
     *
     * @return true;正常状态    false:未添加或正在移除状态
     */
    private boolean getStatus() {
        return (isAdded() && !isRemoving());
    }

    /**
     * 获取actiivty
     *
     * @return BaseActivity
     */
    public BaseActivity getBaseActivity() {
        if (mActivity == null) {
            mActivity = (BaseActivity) getActivity();
        }

        return mActivity;
    }

    /**
     * 显示ProgressDialog
     *
     * @param flag
     * @param message
     */
    public void showProgress(boolean flag, String message) {
        if (getStatus()) {
            BaseActivity activity = getBaseActivity();
            if (activity != null) {
                activity.showProgress(flag, message);
            }
        }
    }

    /**
     * 隐藏ProgressDialog
     */
    public void hideProgress() {
        if (getStatus()) {
            BaseActivity activity = getBaseActivity();
            if (activity != null) {
                activity.hideProgress();
            }
        }
    }

    /**
     * 显示Toast
     *
     * @param info
     */
    public void toast(String info) {
        ToastUtils.show(info);
    }

    public void forward(Class<?> cls) {
        forward(cls, null);
    }

    public void forward(Class<?> cls, Bundle bundle) {
        forward(cls, bundle, -1);
    }

    public void forward(Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent(getActivity(), cls);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        if (requestCode != -1) {
            startActivityForResult(intent, requestCode);
        } else {
            startActivity(intent);
        }
    }

    /**
     * 显示软键盘
     */
    public void showInputSoft() {
        getBaseActivity().showSoftInput();
    }

    /**
     * 隐藏软键盘
     */
    public void hideInputSoft() {
        getBaseActivity().hideSoftInput();
    }

    /**
     * 在初始化控件之前的某些操作，例如：getArguments()
     */
    protected void onBefore() {

    }

    /**
     * 初始化布局
     *
     * @return 布局资源Id
     */
    public abstract int getLayoutRes();

    /**
     * 初始化控件
     */
    public abstract void initUiAndListener();

    /**
     * 初始化控制器
     */
    public abstract void initPresenter();

    /**
     * 初始化一些数据，例如网络数据
     */
    protected abstract void initData();
}
