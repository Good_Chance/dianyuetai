package com.music.dianyuetai.ui;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.interactor.impl.AlertPasswordInteractor;
import com.music.dianyuetai.presenter.IAlertPasswordPresenter;
import com.music.dianyuetai.presenter.impl.AlertPasswordPresenter;
import com.music.dianyuetai.view.AlertPasswordView;

import butterknife.BindView;

public class AlterPasswordActivity extends BaseActivity implements AlertPasswordView, View.OnClickListener {

    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.edit_old_password)
    EditText editOldPassword;
    @BindView(R.id.edit_new_password)
    EditText editNewPassword;
    @BindView(R.id.edit_repeat_password)
    EditText editRepeatPassword;
    @BindView(R.id.text_alert_password)
    TextView textAlertPassword;

    private IAlertPasswordPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_alter_password;
    }

    @Override
    public void initUiAndListener() {
        imageBack.setOnClickListener(this);
        textAlertPassword.setOnClickListener(this);
    }

    @Override
    public void initPresenter() {
        presenter = new AlertPasswordPresenter(this, new AlertPasswordInteractor());
    }

    @Override
    protected void initData() {

    }

    @Override
    public String getOldPassword() {
        return editOldPassword.getText().toString().trim();
    }

    @Override
    public String getNewPassword() {
        return editNewPassword.getText().toString().trim();
    }

    @Override
    public String getRepeatPassword() {
        return editRepeatPassword.getText().toString().trim();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_back:
                finish();
                break;
            case R.id.text_alert_password:
                presenter.alertPassword();
                break;
        }
    }
}
