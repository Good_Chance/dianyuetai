package com.music.dianyuetai.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.marst.mxrecyclerview.SwipRefreshRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.MusicAdapter;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.interactor.impl.MusicInteractor;
import com.music.dianyuetai.presenter.IMusicPresenter;
import com.music.dianyuetai.presenter.impl.MusicPresenter;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.music.dianyuetai.utils.UIUtils;
import com.music.dianyuetai.view.MusicsView;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MusicListFragment extends BaseFragment implements MusicsView {

    @BindView(R.id.recycler_view)
    SwipRefreshRecyclerView recyclerView;

    private MusicAdapter adapter;

    private IMusicPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_music_list;
    }

    @Override
    public void initUiAndListener() {
        adapter = new MusicAdapter(getContext(), null);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setColorSchemeResources(R.color.tomato, R.color.default_background_color);
        recyclerView.setProgressViewOffset(true, UIUtils.dp2Px(50), UIUtils.dp2Px(250));

        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Music>() {
            @Override
            public void onItemClick(View view, int position, Music data) {
                Bundle bundle = new Bundle();
                bundle.putString(Extra.MUSIC_ID, data.getObjectId());
                forward(MusicPlayerActivity.class, bundle);
            }
        });

        recyclerView.setOnLoadingListener(new MxRecyclerView.OnLoadingListener() {
            @Override
            public void onRefresh() {
                presenter.refresh();
            }

            @Override
            public void onLoadMore() {
                presenter.loadMore();
            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new MusicPresenter(this, new MusicInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void setLoadState(int state) {
        RecyclerViewUtils.setLoadState(recyclerView, state);
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void refresh(List<Music> list) {
        adapter.replace(list);
    }

    @Override
    public void loadMore(List<Music> list) {
        adapter.append(list);
    }
}
