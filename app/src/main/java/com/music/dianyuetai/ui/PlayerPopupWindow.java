package com.music.dianyuetai.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.PlayerPopupAdapter;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.event.UpdatePlayerEvent;
import com.music.dianyuetai.player.MusicManager;
import com.music.dianyuetai.utils.DateUtils;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.utils.StringUtils;
import com.music.dianyuetai.widget.AlwaysMarqueeTextView;
import com.music.dianyuetai.widget.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.music.dianyuetai.player.MusicManager.PLAYING;
import static com.music.dianyuetai.player.MusicManager.REPEAT;
import static com.music.dianyuetai.player.MusicManager.REPEAT_ONE;
import static com.music.dianyuetai.player.MusicManager.SHUFFLE;

/**
 * Created by Tang on 2016/5/31 0031.
 */
public class PlayerPopupWindow extends PopupWindow implements View.OnClickListener {
    @BindView(R.id.text_music_title)
    AlwaysMarqueeTextView textMusicTitle;
    @BindView(R.id.text_music_artist)
    TextView textMusicArtist;
    @BindView(R.id.image_play_mode)
    ImageView imagePlayMode;
    @BindView(R.id.image_playlist)
    ImageView imagePlaylist;
    @BindView(R.id.text_current_time)
    TextView textCurrentTime;
    @BindView(R.id.seekbar_music_progress)
    AppCompatSeekBar seekbarMusicProgress;
    @BindView(R.id.text_duration)
    TextView textDuration;
    @BindView(R.id.image_previous)
    ImageView imagePrevious;
    @BindView(R.id.image_play)
    ImageView imagePlay;
    @BindView(R.id.image_next)
    ImageView imageNext;
    @BindView(R.id.layout_music_player)
    LinearLayout layoutMusicPlayer;
    @BindView(R.id.image_back)
    ImageView imageBack;
    @BindView(R.id.text_playlist_clear)
    TextView textPlaylistClear;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.layout_music_list)
    LinearLayout layoutMusicList;
    @BindView(R.id.image_music_picture)
    CircularImageView imageMusicPicture;

    private Activity mContext;
    private View mPpwView;
    private View anchorView;

    //歌曲列表适配器
    private PlayerPopupAdapter adapter;

    //动画持续时间
    private static final int SCALE_DURATION = 300;
    //弹窗播放器动画
    private ScaleAnimation animation1;
    private ScaleAnimation animation2;
    //动画是否结束
    private boolean isAnimationFinish = true;

    private MusicManager musicManager;

    public PlayerPopupWindow(Activity context, View anchorView) {
        super(context);
        this.mContext = context;
        this.musicManager = MusicManager.getInstance();
        this.anchorView = anchorView;
        this.mPpwView = LayoutInflater.from(mContext).inflate(R.layout.ppw_music_player, null);

        setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setBackgroundDrawable(new ColorDrawable());
        setAnimationStyle(R.style.PlayerPpwStyle);
        setContentView(mPpwView);
        setFocusable(true);
        setOutsideTouchable(true);

        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                isAnimationFinish = true;
                //还原窗口透明度
                setWindowAlpha(1f);
                //显示播放窗口，隐藏歌曲列表
                layoutMusicPlayer.setVisibility(View.VISIBLE);
                layoutMusicList.setVisibility(View.GONE);
            }
        });

        update();
        ButterKnife.bind(this, mPpwView);
        initPlayerAnimation();
        initViews();
        initPlayer();
        initPlayerList();
    }

    /**
     * 初始化播放器动画
     */
    private void initPlayerAnimation() {
        Interpolator accelerator = new AccelerateInterpolator();
        Interpolator decelerator = new DecelerateInterpolator();

        animation1 = new ScaleAnimation(1, 0, 1, 1, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
        animation1.setInterpolator(accelerator);
        animation1.setDuration(SCALE_DURATION);
        animation2 = new ScaleAnimation(0, 1, 1, 1, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
        animation2.setDuration(SCALE_DURATION);
        animation2.setInterpolator(decelerator);

        //设置动画监听
        animation1.setAnimationListener(new PlayerAnimationListener());
        animation2.setAnimationListener(new Animation2Listener());
    }

    private void initViews() {
        seekbarMusicProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //seekbar拖动进度条结束，更新歌曲播放进度
                musicManager.seekTo(seekBar.getProgress());
                textCurrentTime.setText(DateUtils.formatPlay(seekBar.getProgress()));
            }
        });

        imageMusicPicture.setOnClickListener(this);
        imagePlayMode.setOnClickListener(this);
        imagePlaylist.setOnClickListener(this);
        imagePrevious.setOnClickListener(this);
        imagePlay.setOnClickListener(this);
        imageNext.setOnClickListener(this);
        imageBack.setOnClickListener(this);
        textPlaylistClear.setOnClickListener(this);
    }

    /**
     * 初始化播放器界面数据
     */
    public void initPlayer() {
        if (!refreshPlayer()) {
            imagePlay.setSelected(false);
            textMusicTitle.setText("");
            textMusicArtist.setText("");
            textCurrentTime.setText("00:00");
            textDuration.setText("00:00");
            seekbarMusicProgress.setProgress(0);
            seekbarMusicProgress.setSecondaryProgress(0);
            ImageUtil.loadImg(imageMusicPicture, R.drawable.ic_launcher);
        }
    }

    /**
     * 刷新播放器信息显示
     *
     * @return
     */
    public boolean refreshPlayer() {
        LocalMusic music = musicManager.getCurrentMusic();
        if (music != null) {
            imagePlay.setSelected(musicManager.getCurrentState() == MusicManager.PLAYING);

            textMusicTitle.setText(music.getName());
            textMusicArtist.setText(music.getArtist());
            ImageUtil.loadImg(imageMusicPicture, music.getSPicUrl());

            return true;
        }

        Log.e("sdfsdfs", "歌单为空");

        return false;
    }

    private void initPlayerList() {
        adapter = new PlayerPopupAdapter(mContext, null);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<LocalMusic>() {
            @Override
            public void onItemClick(View view, int position, LocalMusic data) {
                musicManager.play(position);
                refreshPlayer();
            }
        });

        adapter.setOnClearListener(new PlayerPopupAdapter.OnClearListener() {
            @Override
            public void onClear(int position) {
                adapter.remove(position);
                musicManager.remove(position);
                initPlayer();
            }
        });

        adapter.append(musicManager.getPlayList());
    }

    /**
     * 更新player歌曲列表
     */
    public void updatePlayList() {
        adapter.replace(musicManager.getPlayList());
    }

    /**
     * 设置播放进度
     */
    public void setProgress(long max, long progress) {
        seekbarMusicProgress.setMax((int) max);
        seekbarMusicProgress.setProgress((int) progress);
        textDuration.setText(DateUtils.formatPlay(max));
        textCurrentTime.setText(DateUtils.formatPlay(progress));
    }

    public void updateProgress(UpdatePlayerEvent event) {
        int count = adapter.getItemCount();
        if (count != 0 && event.index < count) {
            LocalMusic data = adapter.getData(event.index);
            if (data.getPlayState() != event.playState) {
                data.setPlayState(event.playState);
                adapter.notifyItemChanged(event.index);
            }

            int position = (int) event.currentPosition;
            int duration = (int) event.duration;
            int bufferPosition = (duration * event.bufferPosition) / 100;

            seekbarMusicProgress.setProgress(position);
            seekbarMusicProgress.setMax(duration);
            seekbarMusicProgress.setSecondaryProgress(bufferPosition);
            textCurrentTime.setText(DateUtils.formatPlay(position));
            textDuration.setText(DateUtils.formatPlay(duration));
        }
    }

    /**
     * 设置播放按钮显示状态
     *
     * @param state true：播放，false：暂停
     */
    public void setPlayBtnState(boolean state) {
        if (musicManager.getCurrentMusic() != null) {
            imagePlay.setSelected(state);
        }
    }

    /**
     * 显示player弹窗
     */
    public void show() {
        setWindowAlpha(0.7f);
        showAtLocation(anchorView, Gravity.CENTER, 0, 0);
    }

    /**
     * 设置窗口透明度
     *
     * @param alpha
     */
    private void setWindowAlpha(float alpha) {
        Window window = mContext.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.alpha = alpha;
        window.setAttributes(attributes);
    }

    @Override
    public void onClick(View view) {
        //翻转动画未执行结束则不触发点击事件
        if (!isAnimationFinish) {
            return;
        }

        switch (view.getId()) {
            case R.id.image_music_picture:
                //进入详细页
                LocalMusic music = musicManager.getCurrentMusic();
                if (music != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Extra.MUSIC_ID, music.getMusicId());
                    Intent intent = new Intent(mContext, MusicPlayerActivity.class);
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                    dismiss();
                }
                break;
            case R.id.image_play_mode:
                //选择播放模式
                int playMode = musicManager.getCurrentMode();
                int iconRes = 0;
                switch (playMode) {
                    case REPEAT:
                        playMode = SHUFFLE;
                        iconRes = R.drawable.ic_shuffle;
                        break;
                    case SHUFFLE:
                        playMode = REPEAT_ONE;
                        iconRes = R.drawable.ic_repeat_one;
                        break;
                    case REPEAT_ONE:
                        playMode = REPEAT;
                        iconRes = R.drawable.ic_repeat;
                        break;
                    default:
                        break;
                }

                musicManager.setCurrentMode(playMode);
                imagePlayMode.setImageResource(iconRes);
                //切换播放模式
                break;
            case R.id.image_playlist:
                //返回播放器
            case R.id.image_back:
                //切换至播放列表
                mPpwView.startAnimation(animation1);
                break;
            case R.id.image_previous:
                //上一首
                musicManager.playPrevious();
                setPlayBtnState(true);
                refreshPlayer();
                break;
            case R.id.image_play:
                //播放/暂停
                if (musicManager.getCurrentMusic() != null) {
                    int state = musicManager.play();
                    setPlayBtnState(state == PLAYING);
                }
                break;
            case R.id.image_next:
                //下一首
                musicManager.playNext();
                setPlayBtnState(true);
                initPlayer();
                break;
            case R.id.text_playlist_clear:
                //清空列表
                adapter.removeAll();
                musicManager.removeAll();
                initPlayer();
                break;
            default:
                break;
        }
    }

    class PlayerAnimationListener implements Animation.AnimationListener {
        @Override
        public void onAnimationStart(Animation animation) {
            isAnimationFinish = false;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (layoutMusicList.getVisibility() == View.GONE) {
                layoutMusicList.setVisibility(View.VISIBLE);
                layoutMusicPlayer.setVisibility(View.GONE);
            } else {
                layoutMusicList.setVisibility(View.GONE);
                layoutMusicPlayer.setVisibility(View.VISIBLE);
            }

            mPpwView.startAnimation(animation2);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }

    class Animation2Listener implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            isAnimationFinish = true;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }
}
