/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * 描述：包含Fragment的Activity基类。
 * 作者：唐志远
 * 日期：2016年06月16日-16时07分
 */
public abstract class BaseFragmentActivity extends BaseActivity {
    private FragmentManager fragmentManager;

    /**
     * 获取fragment管理器
     *
     * @return
     */
    public FragmentManager getBaseFragmentManager() {
        if (fragmentManager == null) {
            fragmentManager = getSupportFragmentManager();
        }

        return fragmentManager;
    }

    /**
     * 获取fragment事务管理
     *
     * @return
     */
    public FragmentTransaction getFragmentTransaction() {
        return getBaseFragmentManager().beginTransaction();
    }

    /**
     * 添加一个fragment
     *
     * @param resId
     * @param fragment
     */
    public void addFragment(int resId, Fragment fragment) {
        if (!fragment.isAdded()) {
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.add(resId, fragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * 添加一个fragment
     *
     * @param resId
     * @param fragment
     * @param tag
     */
    public void addFragment(int resId, Fragment fragment, String tag) {
        if (!fragment.isAdded()) {
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.add(resId, fragment, tag);
            fragmentTransaction.commit();
        }
    }

    /**
     * 移除一个fragment
     *
     * @param fragment
     */
    public void removeFragment(Fragment fragment) {
        if (fragment.isAdded()) {
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * 显示一个fragment
     *
     * @param fragment
     */
    public void showFragment(Fragment fragment) {
        if (fragment.isHidden()) {
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.show(fragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * 隐藏一个fragment
     *
     * @param fragment
     */
    public void hideFragment(Fragment fragment) {
        if (!fragment.isHidden()) {
            FragmentTransaction fragmentTransaction = getFragmentTransaction();
            fragmentTransaction.hide(fragment);
            fragmentTransaction.commit();
        }
    }

    public void replaceFragment(int resId, Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentTransaction();
        fragmentTransaction.replace(resId, fragment).commit();
    }


}
