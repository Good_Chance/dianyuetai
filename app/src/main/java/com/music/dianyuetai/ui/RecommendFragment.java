package com.music.dianyuetai.ui;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.marst.mxrecyclerview.SwipRefreshRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.RecommendAdapter;
import com.music.dianyuetai.bean.BannerBean;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.interactor.impl.RecommendInteractor;
import com.music.dianyuetai.presenter.IRecommendPresenter;
import com.music.dianyuetai.presenter.impl.RecommendPresenter;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.music.dianyuetai.utils.UIUtils;
import com.music.dianyuetai.view.RecommendView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecommendFragment extends BaseFragment implements RecommendView {

    @BindView(R.id.recycler_view)
    SwipRefreshRecyclerView recyclerView;

    private RecommendAdapter adapter;

    private IRecommendPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_recommend;
    }

    @Override
    public void initUiAndListener() {
        adapter = new RecommendAdapter(getActivity(), null, null);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        recyclerView.setLoadingMoreEnabled(false);
        recyclerView.setColorSchemeResources(R.color.tomato, R.color.default_background_color);
        recyclerView.setProgressViewOffset(true, UIUtils.dp2Px(50), UIUtils.dp2Px(250));

        recyclerView.setOnLoadingListener(new MxRecyclerView.OnLoadingListener() {
            @Override
            public void onRefresh() {
                presenter.refresh();
            }

            @Override
            public void onLoadMore() {

            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new RecommendPresenter(this, new RecommendInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void refresh(List<Music> list) {
        adapter.replaceRecommend(list);

        List<BannerBean> bannerBeanList = new ArrayList<>();

        for (Music music : list) {
            BannerBean bannerBean = new BannerBean(music.getName(), music.getResource().getsPicUrl());
            bannerBeanList.add(bannerBean);
        }

        adapter.replaceBanner(bannerBeanList);
    }
}
