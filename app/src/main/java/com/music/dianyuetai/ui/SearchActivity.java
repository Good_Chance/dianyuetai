package com.music.dianyuetai.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.BaseRecyclerAdapter;
import com.music.dianyuetai.adapter.MusicAdapter;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.interactor.impl.SearchInteractor;
import com.music.dianyuetai.presenter.ISearchPresenter;
import com.music.dianyuetai.presenter.impl.SearchPresenter;
import com.music.dianyuetai.utils.RecyclerViewUtils;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

import butterknife.BindView;

public class SearchActivity extends BaseActivity implements com.music.dianyuetai.view.SearchView, View.OnClickListener {

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.recycler_view)
    MxRecyclerView recyclerView;
    @BindView(R.id.text_search)
    TextView textSearch;

    private MusicAdapter adapter;

    private ISearchPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.activity_search;
    }

    @Override
    public void initUiAndListener() {
        textSearch.setOnClickListener(this);

        adapter = new MusicAdapter(this, null);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Music>() {
            @Override
            public void onItemClick(View view, int position, Music data) {
                Bundle bundle = new Bundle();
                bundle.putString(Extra.MUSIC_ID, data.getObjectId());
                forward(MusicPlayerActivity.class, bundle);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setPullRefreshEnabled(false);
        recyclerView.setOnLoadingListener(new MxRecyclerView.OnLoadingListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                presenter.loadMore();
            }
        });

        //设置SearchView字体大小
        SearchView.SearchAutoComplete textView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, AutoUtils.getPercentHeightSizeBigger(30));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    textSearch.setText("取消");
                } else {
                    textSearch.setText("搜索");
                }
                return false;
            }
        });
    }

    @Override
    public void initPresenter() {
        presenter = new SearchPresenter(this, new SearchInteractor());
    }

    @Override
    protected void initData() {

    }

    @Override
    public String getSelection() {
        return searchView.getQuery().toString().trim();
    }

    @Override
    public void setRefreshState(int state) {
        RecyclerViewUtils.setRefreshState(recyclerView, state);
    }

    @Override
    public void setLoadState(int state) {
        RecyclerViewUtils.setLoadState(recyclerView, state);
    }

    @Override
    public void refresh(List<Music> list) {
        adapter.replace(list);
    }

    @Override
    public void loadMore(List<Music> musicList) {
        adapter.append(musicList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_search:
                String text = textSearch.getText().toString();
                if (TextUtils.equals("取消", text)) {
                    finish();
                } else if (TextUtils.equals("搜索", text)) {
                    presenter.search();
                }
                break;
            default:
                break;
        }
    }
}
