package com.music.dianyuetai.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.thread.EventThread;
import com.music.dianyuetai.R;
import com.music.dianyuetai.adapter.FragmentAdapter;
import com.music.dianyuetai.download.DownloadService;
import com.music.dianyuetai.event.LoginEvent;
import com.music.dianyuetai.event.MusicChangeEvent;
import com.music.dianyuetai.event.UpdatePlayerEvent;
import com.music.dianyuetai.interactor.impl.MainInteractor;
import com.music.dianyuetai.player.MusicManager;
import com.music.dianyuetai.player.MusicPlayerService;
import com.music.dianyuetai.presenter.IMainPresenter;
import com.music.dianyuetai.presenter.impl.MainPresenter;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.utils.ToastUtils;
import com.music.dianyuetai.view.MainView;

import butterknife.BindView;

import static com.music.dianyuetai.player.MusicManager.PLAYING;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, MainView {

    @BindView(R.id.tool_bar)
    Toolbar toolBar;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private View noLoginView;
    private View loginView;
    private ImageView imageUser;
    private TextView textUserName;

    //抽屉开关控件
    private ActionBarDrawerToggle mToggle;

    private FragmentAdapter adapter;

    private PlayerPopupWindow playerPopupWindow;
    //音乐播放服务
    private MusicPlayerService playerService;
    //音乐播放管理器
    private MusicManager musicManager;

    private AnimationDrawable playerAnimationDrawable;

    private boolean isBound = false;

    private long exitTime;

    private IMainPresenter presenter;

    //首页tab标签
    private String[] titles = {"推荐", "最新", "分类"};
    private Fragment[] fragments = {
            new RecommendFragment(),
            new MusicListFragment(),
            new CategoryFragment()
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MusicPlayerService.PlayerBinder binder = (MusicPlayerService.PlayerBinder) service;
            playerService = binder.getService();
            musicManager.setPlayer(playerService);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            isBound = false;
        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onBefore(Bundle savedInstanceState) {
        super.onBefore(savedInstanceState);
        RxBus.get().register(this);
        musicManager = MusicManager.getInstance();

        bindService(new Intent(this, MusicPlayerService.class), mConnection, Context.BIND_AUTO_CREATE);
        startService(new Intent(this, DownloadService.class));
    }

    @Override
    public void initUiAndListener() {
        //初始化ToolBar
        initToolBar();
        initDrawerLayout();
        initNavigationView();
        initViewPager();
    }

    @Override
    public void initPresenter() {
        presenter = new MainPresenter(this, new MainInteractor());
    }

    @Override
    protected void initData() {
        presenter.initializes();
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void updatePlayerEvent(MusicChangeEvent event) {
        if (playerPopupWindow != null) {
            playerPopupWindow.refreshPlayer();
            playerPopupWindow.updatePlayList();
            playerPopupWindow.setPlayBtnState(true);
            playerPopupWindow.setProgress(0, 0);
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void updatePlayerProgressEvent(UpdatePlayerEvent event) {
        if (event.playState == PLAYING) {
            if (!playerAnimationDrawable.isRunning()) {
                playerAnimationDrawable.start();
            }
        } else {
            playerAnimationDrawable.selectDrawable(0);
            playerAnimationDrawable.stop();
        }

        if (playerPopupWindow != null) {
            playerPopupWindow.setPlayBtnState(event.playState == PLAYING);
            playerPopupWindow.updateProgress(event);
        }
    }

    @Subscribe(thread = EventThread.MAIN_THREAD)
    public void LoginEvent(LoginEvent event){
        presenter.initializes();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
        presenter.onDestroy();
        musicManager.destory();
        if (isBound) {
            unbindService(mConnection);
            isBound = false;
        }

        if (DownloadService.isServiceRunning(this)) {
            stopService(new Intent(this, DownloadService.class));
        }

        //TODO 检测主页内存泄漏，测试用
        //RefWatcher refWatcher = App.getRefWatcher(this);
        //refWatcher.watch(this);
    }

    private void initViewPager() {
        adapter = new FragmentAdapter(getSupportFragmentManager(), fragments, titles);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
    }

    /**
     * 抽屉控件初始化
     */
    private void initDrawerLayout() {
        // 初始化抽屉开关控件
        mToggle = new ActionBarDrawerToggle(this, drawerLayout, 0, 0);
        // 同步开关与抽屉显示状态
        mToggle.syncState();
        // 设置抽屉监听器，关联抽屉与toggle的显示状态
        drawerLayout.addDrawerListener(mToggle);
    }

    /**
     * 初始化ToolBar
     */
    private void initToolBar() {
        // 设置标题栏
        toolBar.setTitle("电悦台");

        setSupportActionBar(toolBar);
        // 显示actionbar左上角的图标
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * 初始化导航菜单
     */
    private void initNavigationView() {
        // 调侧滑菜单项点击事件
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        noLoginView = headerView.findViewById(R.id.layout_no_login);
        loginView = headerView.findViewById(R.id.layout_login);
        imageUser = (ImageView) headerView.findViewById(R.id.image_user);
        textUserName = (TextView) headerView.findViewById(R.id.text_user_name);

        noLoginView.setOnClickListener(this);
        loginView.setOnClickListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appbar_layout:
                // 选中导航菜单的点击项
                drawerLayout.closeDrawers();
                break;
            case R.id.menu_history:
                //选中历史记录
                forward(HistoryActivity.class);
                break;
            case R.id.menu_favorite:
                //选中我的收藏
                forward(FavoriteActivity.class);
                break;
            case R.id.menu_download:
                //选中下载管理
                forward(DownloadManagerActivity.class);
                break;
            case R.id.menu_dianmiao:
                //选中关于电喵
                break;
            case R.id.menu_author:
                //选中关于作者
                break;
            case R.id.menu_opinion:
                //选中意见反馈
                break;
        }
        // 关闭抽屉
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        playerAnimationDrawable = (AnimationDrawable) menu.findItem(R.id.action_player).getIcon();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // 当点击了左上角actionBar的图标时（抽屉开关控件），打开或关闭抽屉
        switch (item.getItemId()) {
            case android.R.id.home:
                mToggle.onOptionsItemSelected(item);
                return true;
            case R.id.action_download:
                forward(DownloadManagerActivity.class);
                break;
            case R.id.action_search:
                forward(SearchActivity.class);
                return true;
            case R.id.action_player:
                showPlayerPopup();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_no_login:
                forward(LoginActivity.class);
                drawerLayout.closeDrawers();
                break;
            case R.id.layout_login:
                forward(UserActivity.class);
                drawerLayout.closeDrawers();
            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                ToastUtils.show("再按一次退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showPlayerPopup() {
        if (playerPopupWindow == null) {
            playerPopupWindow = new PlayerPopupWindow(this, mLayoutView);
        }
        playerPopupWindow.refreshPlayer();

        playerPopupWindow.show();
    }

    @Override
    public void setUserIcon(String iconUrl) {
        ImageUtil.loadImg(imageUser, iconUrl);
    }

    @Override
    public void setUserName(String userName) {
        textUserName.setText(userName);
    }

    @Override
    public void setNoLoginViewVisible(boolean visible) {
        noLoginView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setLoginViewVisible(boolean visible) {
        loginView.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
