package com.music.dianyuetai.ui;

import com.music.dianyuetai.R;

public class UserActivity extends BaseActivity {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_user;
    }

    @Override
    public void initUiAndListener() {

    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void initData() {

    }
}
