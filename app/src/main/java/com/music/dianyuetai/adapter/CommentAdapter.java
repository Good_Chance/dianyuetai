package com.music.dianyuetai.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.Comment;
import com.music.dianyuetai.bean.User;
import com.music.dianyuetai.utils.DateUtils;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.widget.ShapedImageView;

import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.datatype.BmobDate;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月02日-13时29分
 */

public class CommentAdapter extends BaseRecyclerAdapter<Comment> {

    public CommentAdapter(Context context, List<Comment> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, Comment data) {
        ((ViewHolder) holder).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_comment, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_user_header)
        ShapedImageView imageUserHeader;
        @BindView(R.id.text_user_name)
        TextView textUserName;
        @BindView(R.id.text_comment)
        TextView textComment;
        @BindView(R.id.image_support)
        ImageView imageSupport;
        @BindView(R.id.text_support_count)
        TextView textSupportCount;
        @BindView(R.id.layout_support)
        LinearLayout layoutSupport;
        @BindView(R.id.text_comment_date)
        TextView textCommentDate;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(Comment comment) {
            User user = comment.getUser();
            if (user != null) {
                ImageUtil.loadImg(imageUserHeader, user.getIconUrl());
                textUserName.setText(user.getNickName());
            }

            textComment.setText(comment.getContent());
            textSupportCount.setText(String.valueOf(comment.getSupport()));
            String date = DateUtils.formatOffsetNow(DateUtils.formatBmob(comment.getCreatedAt()));
            textCommentDate.setText(date);
        }
    }
}
