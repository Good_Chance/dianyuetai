package com.music.dianyuetai.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.freegeek.android.materialbanner.MaterialBanner;
import com.freegeek.android.materialbanner.holder.Holder;
import com.freegeek.android.materialbanner.holder.ViewHolderCreator;
import com.freegeek.android.materialbanner.view.indicator.CirclePageIndicator;
import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.BannerBean;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.ui.MusicPlayerActivity;
import com.music.dianyuetai.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-09时37分
 */

public class RecommendAdapter extends RecyclerView.Adapter {
    //轮播图
    public static final int TYPE_BANNER = 0;
    //推荐内容
    public static final int TYPE_RECOMMEND = 1;

    protected Context mContext;
    protected LayoutInflater mInflater;

    private List<BannerBean> bannerList;
    private List<Music> recommendList;

    public RecommendAdapter(Context context, List<BannerBean> bannerList, List<Music> recommendList) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.bannerList = bannerList;
        this.recommendList = recommendList;
    }

    public void replaceRecommend(List<Music> musicList) {
        this.recommendList = musicList;
        notifyDataSetChanged();
    }

    public void replaceBanner(List<BannerBean> bannerList) {
        this.bannerList = bannerList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case TYPE_BANNER:
                view = mInflater.inflate(R.layout.listitem_recommend_banner, parent, false);
                return new BannerViewHolder(view);
            case TYPE_RECOMMEND:
                view = mInflater.inflate(R.layout.listitem_recommend, parent, false);
                return new RecommendViewHolder(view);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BannerViewHolder) {
            ((BannerViewHolder) holder).render(bannerList);
        } else if (holder instanceof RecommendViewHolder) {
            ((RecommendViewHolder) holder).render(recommendList);
        }
    }

    @Override
    public int getItemCount() {
        return (bannerList == null ? 0 : 1) + (recommendList == null ? 0 : 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_BANNER;
        } else if (position == 1) {
            return TYPE_RECOMMEND;
        }

        return super.getItemViewType(position);
    }

    class BannerViewHolder extends BaseViewHolder {
        @BindView(R.id.banner)
        MaterialBanner banner;

        public BannerViewHolder(View itemView) {
            super(itemView);

        }

        public void render(List<BannerBean> list) {

            //TODO 后续需要自定义Banner
            banner.stopTurning();
            banner
                    .setPages(new ViewHolderCreator() {
                        @Override
                        public Object createHolder() {
                            return new ImageHolderView();
                        }
                    }, list)
                    .setIndicator(new CirclePageIndicator(mContext))
                    .setOnItemClickListener(new MaterialBanner.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {

                        }
                    })
                    .setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            //textView.setText("My hometown: page " + ++position);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

            banner.startTurning(4000);
           /* List<String> titles = new ArrayList<>();
            List<String> imageUrls = new ArrayList<>();
            for (BannerBean bannerBean : list) {
                titles.add(bannerBean.getTitle());
                imageUrls.add(bannerBean.getImageUrl());
            }

            banner.setBannerTitles(titles);
            banner.setImages(imageUrls);

            banner.start();*/
        }
    }


    public class ImageHolderView implements Holder<BannerBean> {
        private ImageView imageView;
        private ProgressBar progressBar;

        @Override
        public View createView(Context context) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_banner, null);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            progressBar = (ProgressBar) view.findViewById(R.id.progress);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            progressBar.setVisibility(View.GONE);
            return view;
        }

        @Override
        public void UpdateUI(Context context, int position, BannerBean data) {
            ImageUtil.loadImg(imageView, data.getImageUrl());
           /* Picasso.with(context)
                    .load(data.getUrl())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            progressBar.setVisibility(View.GONE);
                            imageView.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });*/
        }

    }

    class RecommendViewHolder extends BaseViewHolder {
        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;

        private RecommendItemAdapter adapter;

        public RecommendViewHolder(View itemView) {
            super(itemView);

            adapter = new RecommendItemAdapter(mContext, null);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
            recyclerView.setAdapter(adapter);

            adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<Music>() {
                @Override
                public void onItemClick(View view, int position, Music data) {
                    Intent intent = new Intent(mContext, MusicPlayerActivity.class);
                    intent.putExtra("objectId", data.getObjectId());
                    mContext.startActivity(intent);
                }
            });
        }

        public void render(List<Music> musicList) {
            adapter.replace(musicList);
        }
    }
}
