package com.music.dianyuetai.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.dianyuetai.R;

import java.util.List;

import butterknife.BindView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时01分
 */

public class CategoryAdapter extends BaseRecyclerAdapter<List<String>> {

    public CategoryAdapter(Context context, List<List<String>> datas) {
        super(context, datas);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, List<String> data) {
        ((ViewHolder) holder).render(data);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.text_category)
        TextView textCategory;
        @BindView(R.id.text_more)
        TextView textMore;
        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;

        private CategoryItemAdapter adapter;

        public ViewHolder(View itemView) {
            super(itemView);
            adapter = new CategoryItemAdapter(mContext, null);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
            recyclerView.setAdapter(adapter);
        }

        public void render(List<String> categoryList) {
            adapter.replace(categoryList);
        }
    }
}
