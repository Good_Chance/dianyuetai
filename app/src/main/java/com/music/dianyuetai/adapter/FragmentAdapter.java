package com.music.dianyuetai.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.Collection;

/**
 * 描述：ViewPager的FragmentAdapter适配器基类
 * 作者：唐志远
 * 日期：2016年06月22日-08时49分
 */
public class FragmentAdapter extends FragmentStatePagerAdapter {

    private Fragment[] fragments;
    private String[] titles;

    public FragmentAdapter(FragmentManager fm, Collection<Fragment> fragments) {
        super(fm);
        if (fragments != null) {
            fragments.toArray(this.fragments);
        }
    }

    public FragmentAdapter(FragmentManager fm, Collection<Fragment> fragments, Collection<String> titles) {
        this(fm, fragments);
        if (titles != null) {
            titles.toArray(this.titles);
        }
    }

    public FragmentAdapter(FragmentManager fm, Fragment[] fragments) {
        super(fm);
        this.fragments = fragments;
    }

    public FragmentAdapter(FragmentManager fm, Fragment[] fragments, String[] titles) {
        this(fm, fragments);
        this.fragments = fragments;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments == null ? 0 : fragments.length;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titles != null) {
            return titles[position];
        }

        return null;
    }
}