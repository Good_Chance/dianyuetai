/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：RecyclerView适配器基类
 * 作者：唐志远
 * 日期：2016年6月20日
 */
public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected List<T> mDatas = new ArrayList<>();

    //列表项点击事件监听
    public OnItemClickListener onItemClickListener;
    //列表项长按事件监听
    public OnItemLongClickListener onItemLongClickListener;

    public BaseRecyclerAdapter(Context context, List<T> datas) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        if (datas != null) {
            this.mDatas = datas;
        }
    }

    @Override
    public int getItemCount() {
        return mDatas == null ? 0 : mDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        T data = mDatas.get(position);
        initItemClickListener(holder, position, data);
        convert(holder, position, data);
    }

    /**
     * 获取一条数据
     *
     * @param position 数据的索引
     * @return
     */
    public T getData(int position) {
        return mDatas.get(position);
    }

    /**
     * 动态添加一条数据
     *
     * @param data 数据实体类对象
     */
    public void append(T data) {
        if (data != null) {
            mDatas.add(data);
            notifyDataSetChanged();
        }
    }

    /**
     * 动态添加一组数据
     *
     * @param datas 数据实体类集合
     */
    public void append(List<T> datas) {
        if (datas != null && datas.size() > 0) {
            mDatas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    /**
     * 替换全部数据
     *
     * @param datas 数据实体类集合
     */
    public void replace(List<T> datas) {
        mDatas.clear();
        if (datas != null && datas.size() > 0) {
            mDatas.addAll(datas);
            notifyDataSetChanged();
        }
    }

    /**
     * 移除一条数据
     *
     * @param position 数据角标
     */
    public void remove(int position) {
        mDatas.remove(position);
        notifyDataSetChanged();
    }

    /**
     * 移除所有数据
     */
    public void removeAll() {
        mDatas.clear();
        notifyDataSetChanged();
    }

    /**
     * 初始化列表项点击事件
     *
     * @param viewHolder 列表项holder
     * @param position   列表项索引
     */
    private void initItemClickListener(final BaseViewHolder viewHolder, final int position, final T data) {
        if (this.onItemClickListener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, position, data);
                }
            });
        }
        if (this.onItemLongClickListener != null) {
            viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onItemLongClickListener.onItemLongClick(v, position, data);
                    return true;
                }
            });
        }
    }

    /**
     * 显示数据抽象函数
     *
     * @param holder   列表项holder
     * @param position 列表项索引
     */
    protected abstract void convert(BaseViewHolder holder, int position, T data);

    /**
     * 设置列表项点击监听
     *
     * @param listener 点击监听
     */
    public void setOnItemClickListener(OnItemClickListener<T> listener) {
        this.onItemClickListener = listener;
    }

    /**
     * 设置列表项长按监听
     *
     * @param listener 长按监听
     */
    public void setOnItemLongClickListener(OnItemLongClickListener<T> listener) {
        this.onItemLongClickListener = listener;
    }

    /**
     * 列表项监听接口
     *
     * @param <H> 实体类
     */
    public interface OnItemClickListener<H> {
        void onItemClick(View view, int position, H data);
    }

    /**
     * 列表项长按监听接口
     *
     * @param <H> 实体类
     */
    public interface OnItemLongClickListener<H> {
        void onItemLongClick(View view, int position, H data);
    }
}