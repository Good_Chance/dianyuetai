/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zhy.autolayout.utils.AutoUtils;

import butterknife.ButterKnife;

/**
 * 描述：RecyclerView的Holder基类
 * 作者：唐志远
 * 日期：2016年6月20日
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        AutoUtils.autoSize(itemView);
    }
}