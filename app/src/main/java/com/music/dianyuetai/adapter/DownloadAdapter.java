package com.music.dianyuetai.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.local.Download;
import com.music.dianyuetai.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;

import static com.music.dianyuetai.download.DownloadManager.COMPLETED;
import static com.music.dianyuetai.download.DownloadManager.FAILURE;
import static com.music.dianyuetai.download.DownloadManager.LOADING;
import static com.music.dianyuetai.download.DownloadManager.PAUSE;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月14日-13时18分
 */

public class DownloadAdapter extends BaseRecyclerAdapter<Download> {

    public DownloadAdapter(Context context, List<Download> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, Download data) {
        ((ViewHolder) holder).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_download, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_download)
        ImageView imageDownload;
        @BindView(R.id.text_download_name)
        TextView textDownloadName;
        @BindView(R.id.text_progress)
        TextView textProgress;
        @BindView(R.id.btn_download)
        ImageView btnDownload;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(Download download) {
            textDownloadName.setText(download.getMusic().getName());
            ImageUtil.loadImg(imageDownload, download.getMusic().getSPicUrl());

            long progress = download.getDownloadProgress();
            long total = download.getFileSize();

            switch (download.getDownloadState()) {
                case LOADING:
                case PAUSE:
                    String progressStr = formatSize(progress, total);
                    textProgress.setText(progressStr);
                    break;
                case COMPLETED:
                    textProgress.setText("下载完成");
                    break;
                case FAILURE:
                    textProgress.setText("下载异常");
                    break;
                default:
                    break;
            }
        }
    }

    private String formatSize(long finishedSize, long totalSize) {
        StringBuilder sb = new StringBuilder(50);

        float finished = ((float) finishedSize) / 1024 / 1024;
        if (finished < 1) {
            sb.append(String.format("%1$.2f K / ", ((float) finishedSize) / 1024));
        } else {
            sb.append((String.format("%1$.2f M / ", finished)));
        }

        float total = ((float) totalSize) / 1024 / 1024;
        if (total < 1) {
            sb.append(String.format("%1$.2f K ", ((float) totalSize) / 1024));
        } else {
            sb.append(String.format("%1$.2f M ", total));
        }
        return sb.toString();
    }

    public String formatSize(long totalSize) {
        StringBuilder sb = new StringBuilder(50);
        float total = ((float) totalSize) / 1024 / 1024;
        if (total < 1) {
            sb.append(String.format("%1$.2f K ", ((float) totalSize) / 1024));
        } else {
            sb.append(String.format("%1$.2f M ", total));
        }
        return sb.toString();
    }
}
