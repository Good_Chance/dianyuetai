package com.music.dianyuetai.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.widget.ShapedImageView;

import java.util.List;

import butterknife.BindView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月10日-13时59分
 */

public class RecommendItemAdapter extends BaseRecyclerAdapter<Music> {

    public RecommendItemAdapter(Context context, List<Music> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, Music data) {
        ((ViewHolder) holder).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_recommend_item, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_music)
        ShapedImageView imageMusic;
        @BindView(R.id.text_play)
        TextView textPlay;
        @BindView(R.id.text_favorite)
        TextView textFavorite;
        @BindView(R.id.text_music_title)
        TextView textMusicTitle;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(Music music) {
            String imageUrl = music.getResource().getsPicUrl();
            if (!TextUtils.isEmpty(imageUrl)) {
                ImageUtil.loadImg(imageMusic, imageUrl);
            }
            textPlay.setText(String.valueOf(music.getPlayNum()));
            textFavorite.setText(String.valueOf(music.getFavoriteNum()));
            textMusicTitle.setText(music.getName());
        }
    }
}
