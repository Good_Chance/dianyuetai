package com.music.dianyuetai.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.local.LocalHistory;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.utils.DateUtils;
import com.music.dianyuetai.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月20日-14时06分
 */

public class HistoryAdapter extends BaseRecyclerAdapter<LocalHistory> {

    public HistoryAdapter(Context context, List<LocalHistory> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, LocalHistory data) {
        ((ViewHolder) holder).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_history, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_thumbnail)
        ImageView imageThumbnail;
        @BindView(R.id.text_music_title)
        TextView textMusicTitle;
        @BindView(R.id.text_artist)
        TextView textArtist;
        @BindView(R.id.text_date)
        TextView textDate;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(LocalHistory history) {
            LocalMusic localMusic = history.getMusic();
            if (localMusic != null) {
                ImageUtil.loadImg(imageThumbnail, localMusic.getSPicUrl());
                textMusicTitle.setText(localMusic.getName());
                textArtist.setText(localMusic.getArtist());
            }

            textDate.setText(DateUtils.formatOffsetNow(history.getUpdateAt()));
        }
    }
}
