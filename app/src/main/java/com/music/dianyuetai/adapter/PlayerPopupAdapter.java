package com.music.dianyuetai.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.local.LocalMusic;
import com.music.dianyuetai.utils.ResourceUtils;

import java.util.List;

import butterknife.BindView;

import static com.music.dianyuetai.player.MusicManager.PAUSE;
import static com.music.dianyuetai.player.MusicManager.PLAYING;
import static com.music.dianyuetai.player.MusicManager.STOP;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年09月05日-11时05分
 */

public class PlayerPopupAdapter extends BaseRecyclerAdapter<LocalMusic> {

    private OnClearListener mOnClearListener;

    public PlayerPopupAdapter(Context context, List datas) {
        super(context, datas);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_playlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    protected void convert(BaseViewHolder holder, final int position, LocalMusic data) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.render(data);
        if (mOnClearListener != null) {
            viewHolder.imageClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnClearListener.onClear(position);
                }
            });
        }
    }

    /**
     * 设置移除点击事件监听
     *
     * @param listener
     */
    public void setOnClearListener(OnClearListener listener) {
        this.mOnClearListener = listener;
    }

    public interface OnClearListener {
        void onClear(int position);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_bounce)
        ImageView imageBounce;
        @BindView(R.id.text_music_name)
        TextView textMusicName;
        @BindView(R.id.image_clear)
        ImageView imageClear;

        private AnimationDrawable drawable;

        public ViewHolder(View itemView) {
            super(itemView);
            //音乐播放帧动画
            drawable = (AnimationDrawable) ResourceUtils.getDrawable(R.drawable.ic_equalizer_list);
            imageBounce.setImageDrawable(drawable);
        }

        public void render(LocalMusic music) {
            switch (music.getPlayState()) {
                case PLAYING:
                    imageBounce.setVisibility(View.VISIBLE);
                    textMusicName.setSelected(true);
                    drawable.start();
                    break;
                case PAUSE:
                    imageBounce.setVisibility(View.VISIBLE);
                    textMusicName.setSelected(true);
                    if (drawable.isRunning()) {
                        drawable.selectDrawable(0);
                        drawable.stop();
                    }
                    break;
                case STOP:
                default:
                    imageBounce.setVisibility(View.GONE);
                    textMusicName.setSelected(false);
                    if (drawable.isRunning()) {
                        drawable.stop();
                    }
                    break;
            }

            textMusicName.setText(music.getName());
        }
    }
}
