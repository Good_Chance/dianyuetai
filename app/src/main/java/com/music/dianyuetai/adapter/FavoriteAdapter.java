package com.music.dianyuetai.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.music.dianyuetai.R;
import com.music.dianyuetai.bean.Favorite;
import com.music.dianyuetai.bean.Music;
import com.music.dianyuetai.utils.ImageUtil;
import com.music.dianyuetai.utils.okhttp.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;

/**
 * Created by tang on 2016/10/19 0019.
 */

public class FavoriteAdapter extends BaseRecyclerAdapter<Favorite> {

    public FavoriteAdapter(Context context, List<Favorite> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, Favorite data) {
        ((ViewHolder) holder).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_favorite, parent, false);
        return new ViewHolder(view);
    }


    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.image_thumbnail)
        ImageView imageThumbnail;
        @BindView(R.id.text_music_title)
        TextView textMusicTitle;
        @BindView(R.id.text_artist)
        TextView textArtist;
        @BindView(R.id.text_date)
        TextView textDate;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(Favorite favorite) {
            Music music = favorite.getMusic();
            if (music != null) {
                ImageUtil.loadImg(imageThumbnail, music.getResource().getsPicUrl());
                textMusicTitle.setText(music.getName());
                textArtist.setText(music.getArtist());
                textDate.setText(music.getPublishedTime().getDate());
            }
        }
    }
}
