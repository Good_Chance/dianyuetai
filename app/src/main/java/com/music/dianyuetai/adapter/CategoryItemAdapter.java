package com.music.dianyuetai.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.music.dianyuetai.R;

import java.util.List;

import butterknife.BindView;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月11日-11时06分
 */

public class CategoryItemAdapter extends BaseRecyclerAdapter<String> {

    public CategoryItemAdapter(Context context, List<String> datas) {
        super(context, datas);
    }

    @Override
    protected void convert(BaseViewHolder holder, int position, String data) {
        ((ViewHolder) (holder)).render(data);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listitem_category_item, parent, false);
        return new ViewHolder(view);
    }

    class ViewHolder extends BaseViewHolder {
        @BindView(R.id.text_style)
        TextView textStyle;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void render(String category) {
            textStyle.setText(category);
        }
    }
}
