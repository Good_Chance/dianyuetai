package com.music.dianyuetai.utils.okhttp.builder;


import com.music.dianyuetai.utils.okhttp.OkHttpUtils;
import com.music.dianyuetai.utils.okhttp.request.OtherRequest;
import com.music.dianyuetai.utils.okhttp.request.RequestCall;

/**
 * Created by zhy on 16/3/2.
 */
public class HeadBuilder extends GetBuilder {
    @Override
    public RequestCall build() {
        return new OtherRequest(null, null, OkHttpUtils.METHOD.HEAD, url, tag, params, headers, id).build();
    }
}
