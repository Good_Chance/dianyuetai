package com.music.dianyuetai.utils;

import android.content.Context;
import android.widget.Toast;

import com.music.dianyuetai.App;

/**
 * Created by tangzhiyuan on 16/4/24.
 */
public class ToastUtils {
    public static void show(String message) {
        Toast.makeText(App.getAppContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static void show(int stringId) {
        Toast.makeText(App.getAppContext(), stringId, Toast.LENGTH_SHORT).show();
    }
}
