package com.music.dianyuetai.utils;

import com.marst.mxrecyclerview.MxRecyclerView;
import com.marst.mxrecyclerview.SwipRefreshRecyclerView;
import com.music.dianyuetai.constant.LoadState;

/**
 * 描述：类、方法、接口功能描述。
 * 作者：唐志远
 * 日期：2016年10月12日-15时35分
 */

public class RecyclerViewUtils {
    public static void setRefreshState(MxRecyclerView recyclerView, int state) {
        if (state == LoadState.COMPLETE) {
            recyclerView.setRefreshComplete();
        } else if (state == LoadState.FAILURE) {
            recyclerView.setRefreshError();
        } else if (state == LoadState.NOMORE) {
            recyclerView.setRefreshEmpty();
        }
    }

    public static void setRefreshState(SwipRefreshRecyclerView recyclerView, int state) {
        if (state == LoadState.COMPLETE) {
            recyclerView.setRefreshComplete();
        } else if (state == LoadState.FAILURE) {
            recyclerView.setRefreshError();
        } else if (state == LoadState.NOMORE) {
            recyclerView.setRefreshEmpty();
        }
    }

    public static void setLoadState(MxRecyclerView recyclerView, int state) {
        if (state == LoadState.COMPLETE) {
            recyclerView.setLoadMoreComplete();
        } else if (state == LoadState.FAILURE) {
            recyclerView.setLoadMoreError();
        } else if (state == LoadState.NOMORE) {
            recyclerView.setLoadingNoMore(true);
        }
    }

    public static void setLoadState(SwipRefreshRecyclerView recyclerView, int state) {
        if (state == LoadState.COMPLETE) {
            recyclerView.setLoadMoreComplete();
        } else if (state == LoadState.FAILURE) {
            recyclerView.setLoadMoreError();
        } else if (state == LoadState.NOMORE) {
            recyclerView.setLoadingNoMore(true);
        }
    }
}
