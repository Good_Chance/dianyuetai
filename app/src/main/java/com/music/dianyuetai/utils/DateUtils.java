package com.music.dianyuetai.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Tang on 2016/6/1 0001.
 */
public class DateUtils {
    private static SimpleDateFormat musicFormat = new SimpleDateFormat("mm:ss");
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat bmobFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final int SECOND = 1000;
    public static final int MINUTE = 60 * SECOND;
    public static final int HOUR = 60 * MINUTE;
    public static final int DAY = 24 * HOUR;

    public static String formatPlay(long time) {
        return musicFormat.format(new Date(time));
    }

    public static Date formatBmob(String time) {
        Date date = null;
        try {
            date = bmobFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            date = new Date();
        }

        return date;
    }

    public static String formatOffsetNow(Date date) {
        long offTime = System.currentTimeMillis() - date.getTime();
        if (offTime < MINUTE) {
            return offTime / SECOND + " 秒前";
        } else if (offTime < HOUR) {
            return offTime / MINUTE + " 分钟前";
        } else if (offTime < DAY) {
            return offTime / HOUR + " 小时前";
        } else if (offTime < 2 * DAY) {
            return "昨天";
        } else if (offTime < 3 * DAY) {
            return "前天";
        } else {
            return dateFormat.format(date);
        }
    }

    public static String format(Date date){
        return bmobFormat.format(date);
    }
}
