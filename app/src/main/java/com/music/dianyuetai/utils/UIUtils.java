package com.music.dianyuetai.utils;

import android.content.Context;

import com.music.dianyuetai.App;

/**
 * Created by Tang on 2016/5/22 0022.
 */
public class UIUtils {

    public static int dp2Px(float dp) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public static int px2Dp(float px) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }

    public static int dp2Px(int px) {
        float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }

    private static Context getContext() {
        return App.getAppContext();
    }
}
