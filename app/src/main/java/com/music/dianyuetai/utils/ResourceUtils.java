/*
 * ruiqu Copyright (c) 2016.
 */
package com.music.dianyuetai.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.music.dianyuetai.App;

/**
 * 描述：获取res资源工具类。
 * 作者：唐志远
 * 日期：2016年07月22日-13时26分
 */
public class ResourceUtils {

    /**
     * 获取颜色
     *
     * @param colorId 颜色id
     * @return
     */
    public static int getColor(int colorId) {
        return getResources().getColor(colorId);
    }

    /**
     * 获取字符串
     *
     * @param stringId 字符串id
     * @return
     */
    public static String getString(int stringId) {
        return getResources().getString(stringId);
    }

    /**
     * 获取图片drawable
     *
     * @param drawableId 图片资源id
     * @return
     */
    public static Drawable getDrawable(int drawableId) {
        return getResources().getDrawable(drawableId);
    }

    /**
     * 获取Resources对象
     *
     * @return
     */
    private static Resources getResources() {
        return App.getAppContext().getResources();
    }
}
