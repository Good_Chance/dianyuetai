package com.music.dianyuetai.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.music.dianyuetai.App;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 描述：图片相关工具类，提供加载、保存图片方法。
 * 作者：唐志远
 * 日期：2016年06月20日-09时59分
 */
public class ImageUtil {

    /**
     * 通过资源id加载图片
     *
     * @param v     目标ImageView
     * @param resId 图片资源id
     */
    public static void loadImg(ImageView v, int resId) {
        Glide.with(v.getContext())
                .load(resId)
                .placeholder(resId)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(v);
    }

    /**
     * 加载网络图片到指定ImageView
     *
     * @param v   目标ImageView
     * @param url 网络图片url
     */
    public static void loadImg(ImageView v, String url) {
        Glide.with(v.getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(v);
    }

    /**
     * 加载网络图片，请求失败显示error图片
     *
     * @param v          目标ImageView
     * @param url        网络图片url
     * @param errorResId error图片id
     */
    public static void loadImg(ImageView v, String url, int errorResId) {
        Glide.with(v.getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(errorResId)
                .into(v);
    }



    /**
     * 清除图片缓存
     */
    public static void clearImageCache() {
        Glide.get(App.getAppContext()).clearDiskCache();
        Glide.get(App.getAppContext()).clearMemory();
    }

    /**
     * drawable转bitmap
     *
     * @param drawable
     * @return
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * bitmap旋转90度
     *
     * @param bitmap
     * @return
     */
    public static Bitmap transformOrientation(Bitmap bitmap) {
        Matrix matrix = new Matrix();

        matrix.setRotate(90, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

        float targetX = bitmap.getHeight();
        float targetY = 0;

        final float[] values = new float[9];
        matrix.getValues(values);

        float x1 = values[Matrix.MTRANS_X];
        float y1 = values[Matrix.MTRANS_Y];

        matrix.postTranslate(targetX - x1, targetY - y1);

        Bitmap bm1 = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(bm1);
        canvas.drawBitmap(bitmap, matrix, paint);

        return bm1;
    }

    /**
     * 保存图片bitmap至本地
     *
     * @param bm       需要存储的图片bitmap
     * @param fileName 文件名
     * @throws IOException IO异常
     */
    public static String saveFile(Bitmap bm, String fileName) {
        String s = Environment.getExternalStorageDirectory().toString();
        File dirFile = new File(s + "/DCIM/Camera/");
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        File myCaptureFile = new File(s + "/DCIM/Camera/" + fileName);
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(
                    new FileOutputStream(myCaptureFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        try {
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myCaptureFile.getAbsolutePath();
    }
}
